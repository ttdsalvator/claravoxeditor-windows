//
//  RandomName.swift
//  Claravox Editor
//
//  Created by Connor Golden on 9/11/20.
//  Copyright © 2020 Moog Music. All rights reserved.
//

import SwiftUI

class RandomName {
    
    var nouns = [String]()
    var adjectives = [String]()
    var verbs = [String]()
    
    init() {
        do {
            if let filepath = Bundle.main.path(forResource: "nouns", ofType: "txt") {
                let file = try String(contentsOfFile: filepath)
                nouns = file.components(separatedBy: "\n")
            }
        } catch let error {
            print(error.localizedDescription)
        }
        do {
            if let filepath = Bundle.main.path(forResource: "adjectives", ofType: "txt") {
                let file = try String(contentsOfFile: filepath)
                adjectives = file.components(separatedBy: "\n")
            }
        } catch let error {
            print(error.localizedDescription)
        }
        do {
            if let filepath = Bundle.main.path(forResource: "verbs", ofType: "txt") {
                let file = try String(contentsOfFile: filepath)
                verbs = file.components(separatedBy: "\n")
            }
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    func getRandomName() -> String {
        let randomInt = Int.random(in: 0..<3)
        let adjective = adjectives.randomElement() ?? ""
        let noun = nouns.randomElement() ?? ""
        let verb = verbs.randomElement() ?? ""
        switch (randomInt) {
        case 0 :
            return (adjective + " " + noun)
        case 1 :
            return (noun + " " + verb)
        default :
            return (adjective + " " + noun + " " + verb)
        }
    }  
}
