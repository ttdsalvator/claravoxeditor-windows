﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Claravox.Data
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using J = Newtonsoft.Json.JsonPropertyAttribute;
    using R = Newtonsoft.Json.Required;
    using N = Newtonsoft.Json.NullValueHandling;
    using Windows.Storage;
    using Claravox.Models;

    public partial class PresetLite
    {
        public StorageFile file_;
        bool isFavorite_;

        [J("factory")] public bool Factory { get; set; }
        [J("tags")] public string[] TagsArray { get; set; }
        [J("name")] public string Name { get; set; }
        [J("version")] public long Version { get; set; }

        public string Tags
        {
            get { return string.Join(" ", TagsArray); }
        }

        public string Author
        {
            get { return Factory ? "Moog" : "User"; }
        }
    }

    public partial class PresetLite
    {
        public static PresetLite FromJson(string json) => JsonConvert.DeserializeObject<PresetLite>(json, Claravox.Data.Converter.Settings);
    }

    public partial class Preset
    {
        [J("factory")] public bool Factory { get; set; }
        [J("tags")] public string[] TagsArray { get; set; }
        [J("parameters")] public Dictionary<string, double> Parameters { get; set; }
        [J("name")] public string Name { get; set; }
        [J("version")] public long Version { get; set; }

        public string Tags
        { 
            get { return string.Join(" ", TagsArray); }
        }
        
        public string Author
        {
            get { return Factory ? "Moog" : "User"; }
        }
    }

    public partial class Preset
    {
        public static Preset FromJson(string json) => JsonConvert.DeserializeObject<Preset>(json, Claravox.Data.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this Preset self) => JsonConvert.SerializeObject(self, Claravox.Data.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
