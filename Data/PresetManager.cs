﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Claravox.Data;
using System;
using System.IO;
using Windows.Data.Json;
using Windows.Storage;
using System.Collections.ObjectModel;
using System.ComponentModel;
using Windows.UI.Xaml.Controls;
using Windows.UI.Core;
using Windows.Storage.FileProperties;
using System.Globalization;

namespace Claravox.Models
{
    public class PresetManager : Page, INotifyPropertyChanged
    {
        // ----------- Variables ---------------
        public ObservableCollection<StorageFile> presetsFilesArray = new ObservableCollection<StorageFile>();
        public ObservableCollection<PresetLite> presetsLiteArray = new ObservableCollection<PresetLite>();
        public List<String> tags = new List<String>();
        private Preset emptyPreset = new Preset();
        //String searchedText = "";
        int focusedIndex = 0;
        bool isEmpty = true;
        public int FocusedIndex { get; set; }
        public int FocusedPreset { get; set; }

        public Preset focusedPreset;
        public StorageFile focusedPresetFile;

        MidiState midiStatePtr_;

        // ----------- Methods -----------------
        public PresetManager()
        {
            initialize();
        }

        public async void initialize()
        {
            focusedPreset = new Preset();

            readPresets(true);

            var task = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                if (this.presetsFilesArray.Count > 0)
                {
                    this.focusPreset(0);
                }
            });
        }

        public String FocusedPresetName
        {
            get
            {
                return focusedPreset.Name;
            }
            set
            {
                focusedPreset.Name = value;
                OnPropertyChanged("FocusedPresetName");
            }
        }

        public StorageFile FocusedPresetFile
        {
            get
            {
                return focusedPresetFile;
            }
            set
            {
                focusedPresetFile = value;
                OnPropertyChanged("FocusedPresetFile");
            }
        }

        public String FocusedPresetModification
        {
            get; set;
        }

        public List<String> AllPresetsTags
        {
            get
            {
                return tags;
            }
            set
            {
                tags = value;
                OnPropertyChanged("AllPresetsTags");
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public void focusPresetIndex(int index)
        {
            if (index <= presetsFilesArray.Count)
            {
                focusedIndex = index;
                decodePreset(presetsFilesArray[focusedIndex]);
            }
        }

        public void focusPreset(String path)
        {
            foreach (StorageFile file in presetsFilesArray)
            {
                if (file.Path == path)
                {
                    focusedIndex = presetsFilesArray.IndexOf(file);
                    decodePreset(file);
                }
            }
        }

        public void nextPreset()
        {
            int newIndex = focusedIndex + 1;
            if (newIndex < presetsFilesArray.Count)
            {
                focusPresetIndex(newIndex);
            }
        }

        public void prevPreset()
        {
            int newIndex = focusedIndex - 1;
            if (0 <= newIndex && newIndex < presetsFilesArray.Count())
            {
                focusPreset(newIndex);
            }
        }

        public void appendPreset(PresetLite p, StorageFile f)
        {
            p.file_ = f;
            presetsLiteArray.Add(p);

            foreach (String tag in p.TagsArray)
            {
                if (!AllPresetsTags.Contains(tag))
                {
                    AllPresetsTags.Add(tag);
                }
            }
        }

        public async void readPresets(bool initIfNotEmpty)
        {
            StorageFolder storageFolder = ApplicationData.Current.LocalFolder;
            StorageFolder presetsFolder = await storageFolder.GetFolderAsync("Presets");

            IReadOnlyList<StorageFile> fileList = await presetsFolder.GetFilesAsync();
            presetsFilesArray.Clear();
            presetsLiteArray.Clear();

            foreach (StorageFile file in fileList)
            {
                presetsFilesArray.Add(file);
                {
                    String data = await Windows.Storage.FileIO.ReadTextAsync(file);
                    PresetLite p = PresetLite.FromJson(data);
                    if (p != null)
                    {
                        appendPreset(p, file);
                    }
                }
            }

            if (presetsFilesArray.Count > 0)
            {
                isEmpty = false;
            }

            if (initIfNotEmpty && !isEmpty)
            {
                this.focusPreset(0);

                syncFocusedPreset();
            }

            OnPropertyChanged("PresetListChanged");
        }

        private async void decodePreset(StorageFile file)
        {
            System.Diagnostics.Debug.WriteLine("DECODE " + file.Name);

            String text = await Windows.Storage.FileIO.ReadTextAsync(file);
            JsonValue jsonValue = JsonValue.Parse(text);
            Preset decodedPreset = Preset.FromJson(text);
            if (decodedPreset != null)
            {
                focusedPresetFile = file;
                LoadPreset(decodedPreset);
            }
        }

        private async void encodePreset(StorageFile file)
        {
            String text = await Windows.Storage.FileIO.ReadTextAsync(file);
            JsonValue jsonValue = JsonValue.Parse(text);
            Preset decodedPreset = Preset.FromJson(text);
            if (decodedPreset != null)
            {
                focusedPresetFile = file;
                LoadPreset(decodedPreset);
            }
        }

        private void updateFocusetPresetModification()
        {
            // sync modification date
            String path = focusedPresetFile.Path;
            if (File.Exists(path))
            {
                DateTime dt = File.GetLastWriteTime(path);

                FocusedPresetModification = dt.ToString();
                OnPropertyChanged("FocusedPresetModification");
            }

            //BasicProperties basicProperties = await file.GetBasicPropertiesAsync();
            // TODO:
            //{
            //    CultureInfo[] cultures = new CultureInfo[] {CultureInfo.InvariantCulture,
            //                           new CultureInfo("en-us"),
            //                           new CultureInfo("fr-fr")};

            //    String txt = basicProperties.DateModified.ToString(cultures[0]);
            //}

            //FocusedPresetModification = basicProperties.DateModified.ToString("d");
            //OnPropertyChanged("FocusedPresetModification");
        }

        private async Task<PresetLite> decodePresetLite(StorageFile file)
        {
            String text = await Windows.Storage.FileIO.ReadTextAsync(file);
            JsonValue jsonValue = JsonValue.Parse(text);
            PresetLite p = PresetLite.FromJson(text);
            return p;
        }

        public void LoadPreset(Preset p)
        {
            focusedPreset = p;
            updateMidiState();
            syncFocusedPreset();
            OnPropertyChanged("FocusedPresetName");
            OnPropertyChanged("FocusedPresetFile");
            OnPropertyChanged("PreviousEnabled");
            OnPropertyChanged("NextEnabled");
        }

        public void focusPreset(int index)
        {
            if (index <= presetsFilesArray.Count)
            {
                focusedIndex = index;
                decodePreset(presetsFilesArray[focusedIndex]);
            }
        }

        public bool isFirstPreset()
        {
            return focusedIndex == 0;
        }

        public bool isLastPreset()
        {
            return focusedIndex + 1 == presetsFilesArray.Count;
        }

        public bool PreviousEnabled
        {
            get { return !isFirstPreset(); }
        }

        public bool NextEnabled
        {
            get { return !isLastPreset(); }
        }

        private void updateMidiState()
        {
            var task = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                if (this.midiStatePtr_ != null)
                {
                    this.midiStatePtr_.flushPreset();
                }
            });


            //let nc = NotificationCenter.default
            // nc.post(name: Notification.Name("FlushPreset"), object: nil)
        }

        public void configure(MidiState midiState)
        {
            midiStatePtr_ = midiState;
        }

        //  MARK: Deleting

        //    func deletePreset(url: URL)
        //    {
        //        if !isEmpty
        //        {
        //            do
        //            {
        //                removeFavoriteKey(name: focusedPreset().preset.name)


        //            let inRange = 0.. < presets.count
        //                if inRange.contains(focusedIndex - 1) {
        //                    focusedIndex -= 1
        //            }
        //                else
        //                {
        //                    focusedIndex = 0
        //                }

        //                try FileManager.default.removeItem(at: url)


        //            usleep(200000)
        ////                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
        //                _ = self.focusPreset(index: self.focusedIndex)
        //                    self.syncFocusedPreset()
        ////                }
        //        }
        //                catch
        //                {
        //                    print(error)
        //                }
        //            }
        //}


        // MARK: i/o
        public async void deleteFocusedPreset()
        {
            if (presetsFilesArray.Count > 0)
            {
                removeFavoriteKey(focusedPreset.Name);

                await focusedPresetFile.DeleteAsync();
                if (isFirstPreset())
                    nextPreset();
                else
                    prevPreset();

                readPresets(false);
            }
        }

        public async void overwritePresetFile()
        {
            if (focusedPresetFile != null)
            {
                String prev_path = focusedPresetFile.Path;
                String new_name = FocusedPresetName;

                focusedPreset.Factory = false;
                String data = focusedPreset.ToJson();

                await focusedPresetFile.RenameAsync(FocusedPresetName);
                await Windows.Storage.FileIO.WriteTextAsync(focusedPresetFile, data);
                readPresets(false);

                syncFocusedPreset();

                //                focusedPresetFile

                //                self.preset?.group = "User"
                //            let oldUrl = focusedPreset().url
                //            var newUrl = focusedPreset().url.deletingLastPathComponent()
                //            newUrl.appendPathComponent(focusedPreset().preset.name)
                //            newUrl.appendPathExtension("mpr")
                //            encodePreset(preset: self.preset!, url: newUrl)


                //            if oldUrl != newUrl {
                //                    do
                //                    {
                //                        try FileManager.default.removeItem(at: oldUrl)
                //                    }
                //                        catch
                //                        {
                //                            print(error)
                //                        }
                //                        syncPresetList()
                //                }

                //                usleep(200000)
                ////            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                //                self.focusPreset(url: newUrl)
                ////            }
            }
        }

        public void handleRenameFavorite(String oldName, String newName)
        {
            if (getFavorite(oldName))
            {
                removeFavoriteKey(oldName);
                if (!getFavorite(newName))
                {
                    toggleFavorite(newName);
                }
            }
        }
        // MARK: Sync

        private void syncFocusedPreset()
        {
            if (focusedPresetFile != null)
            {
                //this.preset.name = focusedPreset().preset.name
                //self.preset?.tags = focusedPreset().preset.tags

                // TODO: sync favorite
                OnPropertyChanged("FocusedPresetFavorite");

                updateFocusetPresetModification();
            }
        }

        private void handleFolderChange()
        {
            //DispatchQueue.main.async {
            //    [weak self] in
            //self?.syncPresetList()
            //    self?.syncFocusedPreset()
            //}
        }

        private void syncPresetList()
        {
            // TODO: this is rudementary, perhaps implement a search
            //presets.removeAll()
            //isEmpty = true;
            readPresets(false);
            //    if presets.count == 0 {
            //        presets.append(emptyPreset)
            //}
        }

        //private func updateModManager()
        //{
        //    NotificationCenter.default.post(name: Notification.Name("UpdateModManager"), object: nil)
        //}

        //private func sortPresets()
        //{
        //    presets.localizedStandardSort(\.preset.name, ascending: true)
        //}

        // MARK: Favorite
        public void removeFavoriteKey(String name)
        {
            UserDefaults defaults = UserDefaults.Current;
            if (defaults != null)
            {
                if (defaults.isKeyPresentInUserDefaults(name))
                {
                    defaults.Remove(name);
                }
            }
        }

        public bool getFavorite(String name)
        {
            bool is_favorite = false;

            UserDefaults defaults = UserDefaults.Current;
            if (defaults != null)
                if (defaults.isKeyPresentInUserDefaults(name))
                {
                    is_favorite = defaults.Read<bool>(name);
                }

            return is_favorite;
        }

        public void toggleFavorite(String name)
        {
            bool is_favorite = false;

            UserDefaults defaults = UserDefaults.Current;
            if (defaults != null)
            {
                if (defaults.isKeyPresentInUserDefaults(name))
                {
                    is_favorite = defaults.Read<bool>(name);
                    if (is_favorite)
                    {
                        defaults.Remove(name);
                    }
                    else
                    {
                        defaults.Store(name, true);
                    }
                }
                else
                {
                    defaults.Store(name, true);
                }
            }
        }

        // ------------------TAGS------------------
        // MARK: Tags and Filters

        /*
        func addTag(_ newTag: String)
        {
            var exists = false
            for tag in tags {
                if newTag == tag.name {
                    exists = true
                }
            }
            if !exists {
                tags.append(ManagedTag(name: newTag))
            }
        }

        func deleteTag(_ tagName: String)
        {
            for tag in tags {
                if tag.name == tagName {
                    if let index = tags.firstIndex(of: tag) {
                        tags.remove(at: index)
                    }
                }
            }

            for preset in presets {
                if presetHasTag(preset, tagName: tagName) {
                    removeTagFromPreset(preset, tagName: tagName)
                }
            }
        }

          */
        public void removeTag(String oldTag)
        {
            foreach (String tag in tags)
            {
                if (oldTag == tag)
                {
                    tags.Append(oldTag);
                }
            }
        }


        public int getTagCount()
        {
            return tags.Count;
        }

        /*
        func toggleIsSearched(_ tagName: String)
        {
            for tag in tags {
                if tagName == tag.name {
                    let index = tags.firstIndex(of: tag)
                    var tagSearched = tag.isSearched
                    tagSearched.toggle()
                    let new_tag = ManagedTag(name: tagName, isSearched: tagSearched)


                    tags[index!] = new_tag
                    break
                }
            }
        }

        func focusedHasTag(_ thisTag: String) -> Bool {
            var exists = false
            if let tags = focusedPreset().preset.tags {
                for tag in tags {
                    if thisTag == tag {
                        exists = true
                    }
                }
            }
            return exists
        }

        func presetHasTag(_ preset: PresetFile, tagName: String) -> Bool
    {
        var exists = false
            if let tags = preset.preset.tags {
            for tag in tags {
                if tagName == tag {
                    exists = true
                    }
            }
        }
        return exists
        }

    func addTagToFocused(_ newTag: String) {
        if focusedPreset().preset.tags?.count ?? 0 <= 2 {
            if !focusedHasTag(newTag){
                presets[focusedIndex].preset.tags?.append(newTag)
                }
        }
    }

    func removeTagFromFocused(_ oldTag: String) {
        if focusedHasTag(oldTag) {
            presets[focusedIndex].preset.tags?.removeAll { $0 == oldTag }
        }
    }

    func removeTagFromPreset(_ preset: PresetFile, tagName: String) {
        if presetHasTag(preset, tagName: tagName) {
            if let presetIndex = presets.firstIndex(of: preset) {
                presets[presetIndex].preset.tags?.removeAll { $0 == tagName }
            }
        }
    }
    */

        // Favorites

        public bool IsFocusedPresetFavorite()
        {
            bool is_favorite = false;

            UserDefaults defaults = UserDefaults.Current;
            if (defaults != null)
                if (defaults.isKeyPresentInUserDefaults(FocusedPresetName))
                {
                    is_favorite = defaults.Read<bool>(FocusedPresetName);
                }

            return is_favorite;
        }

        public void toggleFocusedPresetFavorite()
        {
            bool is_favorite = false;

            UserDefaults defaults = UserDefaults.Current;
            if (defaults != null)
                if (defaults.isKeyPresentInUserDefaults(FocusedPresetName))
                {
                    is_favorite = defaults.Read<bool>(FocusedPresetName);
                    if (is_favorite)
                    {
                        defaults.Remove(FocusedPresetName);
                    }
                    else
                    {
                        defaults.Store(FocusedPresetName, true);
                    }
                }
                else
                {
                    defaults.Store(FocusedPresetName, true);
                }
        }

        public void setFocusedPresetFavorite(bool state)
        {
            bool is_favorite = false;

            UserDefaults defaults = UserDefaults.Current;
            if (defaults != null)
            {
                if (state)
                {
                    defaults.Remove(FocusedPresetName);
                }
                else
                {
                    defaults.Store(FocusedPresetName, true);
                }
            }
        }
    }
}
