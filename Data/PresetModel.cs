﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using Claravox.Data;

namespace Claravox.Models
{
    public class PresetModel: BindableBase
    {
        String path_;
        String name_;
        bool isFavorite_;

        public PresetModel(Windows.Storage.StorageFile file, bool state)
        {
            path_ = file.Path;
            Name = file.Name;
            IsFavorite = getFavorite(Name);
        }

        public string Name
        {
            get { return name_; }
            set
            {
                name_ = value;
                OnPropertyChanged("Name");
            }
        }

        public bool IsFavorite
        {
            get { return isFavorite_; }
            set
            {
                isFavorite_ = value;
                OnPropertyChanged("IsFavorite");
            }
        }

        public List<string> Tags { get; set; }

        bool getFavorite(String name)
        {
            bool is_favorite = false;
            
            UserDefaults settings = UserDefaults.Current;
            if (settings != null)
                if (settings.isKeyPresentInUserDefaults("name"))
                {
                    is_favorite = settings.Read<bool>("name");
                }

                return is_favorite;
        }

        List<String> getTags(PresetLite presetLite)
        {
            List<String> list = new List<String>();
       
            return list;
        }
    }
}
