﻿//
// VolumeModule.xaml.h
// Declaration of the VolumeModule class
//

#pragma once

#include "VolumeModule.g.h"

namespace ClaravoxEditor
{
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class VolumeModule sealed
	{
	public:
		VolumeModule();
	};
}
