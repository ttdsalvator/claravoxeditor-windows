﻿//
// EditorPercentageSlider.xaml.h
// Declaration of the EditorPercentageSlider class
//

#pragma once

#include "EditorPercentageSlider.g.h"

namespace ClaravoxEditor
{
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class EditorPercentageSlider sealed
	{
	public:
		EditorPercentageSlider();
	};
}
