﻿//
// Osc2Module.xaml.h
// Declaration of the Osc2Module class
//

#pragma once

#include "Osc2Module.g.h"

namespace ClaravoxEditor
{
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class Osc2Module sealed
	{
	public:
		Osc2Module();
	};
}
