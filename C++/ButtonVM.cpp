#include "pch.h"
#include "ButtonVM.h"


using namespace ClaravoxEditor;
ButtonVM::ButtonVM(String^ title, String^ name, bool isSelected)
{
    Title = title;
    Name = name;
    IsSelected = isSelected;
}

String^ ButtonVM::Title::get()
{
    return _value;
}

void ButtonVM::Title::set(String^ value)
{
    _value = value;
    OnPropertyChanged("Title");
}

String^ ButtonVM::Name::get()
{
    return _name;
}

void ButtonVM::Name::set(String^ value)
{
    _name = value;
    OnPropertyChanged("Name");
}

bool ButtonVM::IsSelected::get()
{
    return _isSelected;
}

void ButtonVM::IsSelected::set(bool value)
{
    _isSelected = value;
    OnPropertyChanged("IsSelected");
}



