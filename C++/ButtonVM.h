#pragma once
#include<BaseViewModel.h>
using namespace Platform;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::Foundation::Collections;

namespace ClaravoxEditor {
    [Windows::UI::Xaml::Data::Bindable]
    [Windows::Foundation::Metadata::WebHostHidden]
    public ref class ButtonVM sealed : BaseViewModel {
    public:
        ButtonVM(String^ title, String^ name, bool isSelected);

        property String^ Title
        {
            String^ get();
            void set(String^ value);
        }

        property String^ Name
        {
            String^ get();
            void set(String^ value);
        }

        property bool IsSelected {
            bool get();
            void set(bool value);
        }

private:
    String^ _value;
    String^ _name;
    bool _isSelected;
};
}


