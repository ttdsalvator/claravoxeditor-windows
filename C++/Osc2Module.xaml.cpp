﻿//
// Osc2Module.xaml.cpp
// Implementation of the Osc2Module class
//

#include "pch.h"
#include "Osc2Module.xaml.h"

using namespace ClaravoxEditor;

using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

Osc2Module::Osc2Module()
{
	InitializeComponent();
}
