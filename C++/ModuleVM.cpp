#include "pch.h"
#include "ModuleVM.h"
#include <ParamIntViewModel.h>

using namespace ClaravoxEditor;
using namespace Platform::Collections;

ModuleVM::ModuleVM()
{
}

void ModuleVM::init(String^ title)
{
    Title = title;
    ParamName = Title + " Param";

    ParamsVMList = ref new Vector<ParamBaseViewModel^>();
    ButtonVMList = ref new Vector<ButtonVM^>();

    addIntParameter(0, "Name1", 0, 100, 10);
    addIntParameter(1, "Name2", 0, 100, 10);

    ButtonVMList->Append(ref new ButtonVM("A", ParamName, true));
    ButtonVMList->Append(ref new ButtonVM("B", ParamName, false));
    ButtonVMList->Append(ref new ButtonVM("C", ParamName, false));
    ButtonVMList->Append(ref new ButtonVM("D", ParamName, false));
}

void ModuleVM::addIntParameter(int index, String^ name, int min, int max, int defaultVal)
{
    ParamIntViewModel^ param = ref new ParamIntViewModel();
    param->Name = name;
    param->Min = min;
    param->Max = max;
    param->Value = defaultVal;

    ParamsVMList->Append(param);
}

String^ ModuleVM::Title::get()
{
    return _value;
}

void ModuleVM::Title::set(String^ value)
{
    _value = value;
    OnPropertyChanged("Title");
}

String^ ModuleVM::ParamName::get()
{
    return _paramName;
}

void ModuleVM::ParamName::set(String^ value)
{
    _paramName = value;
    OnPropertyChanged("ParamName");
}

IObservableVector<ParamBaseViewModel^>^ ModuleVM::ParamsVMList::get() 
{
    return _params;
}

void ModuleVM::ParamsVMList::set(IObservableVector<ParamBaseViewModel^>^ value) {
    _params = value;
    OnPropertyChanged("ParamsVMList");
}

IObservableVector<ButtonVM^>^ ModuleVM::ButtonVMList::get() 
{
    return _buttonVMList;
}

void ModuleVM::ButtonVMList::set(IObservableVector<ButtonVM^>^ value) {
    _buttonVMList = value;
    OnPropertyChanged("ButtonVMList");
}