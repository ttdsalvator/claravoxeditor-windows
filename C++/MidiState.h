// "MidiState.h"

#pragma once
#include <ParamIntViewModel.h>
#include <ParamStringViewModel.h>
#include <ParamBaseViewModel.h>
#include <ParametersListVM.h>
#include <DelegateCommand.h>
#include <PageType.h>
#include <ViewModelLocator.h>
#include <BaseViewModel.h>
#include <ModuleVM.h>
#include <ButtonVM.h>
using namespace Platform;
using namespace Windows::UI::Xaml::Data;
using namespace Platform::Collections;
using namespace Windows::Foundation::Collections;

namespace ClaravoxEditor {

    public delegate void PageNavigateEventHandler(int page, Platform::Object^ parameter);

    [Windows::UI::Xaml::Data::Bindable]
    [Windows::Foundation::Metadata::WebHostHidden]
	public ref class  MidiState sealed : Windows::UI::Xaml::Data::INotifyPropertyChanged
	{
    public:
        MidiState();
        void Save(Platform::Object^ parameter);
        void SaveAs(Platform::Object^ parameter);
        void NextPreset(Platform::Object^ parameter); 
        void PrevPreset(Platform::Object^ parameter); 
        void ShowMore(Platform::Object^ parameter); 
        void NavigateToParameters(Platform::Object^ parameter);
        void NavigateToLibrary(Platform::Object^ parameter);
        void NavigateToSettings(Platform::Object^ parameter);

        virtual event Windows::UI::Xaml::Data::PropertyChangedEventHandler^ PropertyChanged;

        property bool IsOpen
        {
            bool get();
            void set(bool value);
        }
        property double Width
        {
            double get();
            void set(double value);
        }
        property double ActualWidth
        {
            double get();
            void set(double value);
        }

        property Windows::UI::Xaml::Input::ICommand^ SaveCommand
        {
            Windows::UI::Xaml::Input::ICommand^ get();
        }
        property Windows::UI::Xaml::Input::ICommand^ SaveAsCommand
        {
            Windows::UI::Xaml::Input::ICommand^ get();
        }
        property Windows::UI::Xaml::Input::ICommand^ NextPresetCommand
        {
            Windows::UI::Xaml::Input::ICommand^ get();
        }
        property Windows::UI::Xaml::Input::ICommand^ PrevPresetCommand
        {
            Windows::UI::Xaml::Input::ICommand^ get();
        }
        property Windows::UI::Xaml::Input::ICommand^ ShowMoreCommand
        {
            Windows::UI::Xaml::Input::ICommand^ get();
        }
        property Windows::UI::Xaml::Input::ICommand^ NavigateParametersCommand
        {
            Windows::UI::Xaml::Input::ICommand^ get();
        }
        property Windows::UI::Xaml::Input::ICommand^ NavigateLibraryCommand
        {
            Windows::UI::Xaml::Input::ICommand^ get();
        }
        property Windows::UI::Xaml::Input::ICommand^ NavigateSettingsCommand
        {
            Windows::UI::Xaml::Input::ICommand^ get();
        }

       
    internal:
        virtual event PageNavigateEventHandler^ NavigateToPage;
    private:
        bool _isOpen;
        double _actualWidth;
        double _width;

        Windows::UI::Xaml::Input::ICommand^ m_saveCommand;
        Windows::UI::Xaml::Input::ICommand^ m_saveAsCommand;
        Windows::UI::Xaml::Input::ICommand^ m_nextPresetCommand;
        Windows::UI::Xaml::Input::ICommand^ m_prevPresetCommand;
        Windows::UI::Xaml::Input::ICommand^ m_showMoreCommand;
        Windows::UI::Xaml::Input::ICommand^ m_navigateParametersCommand;
        Windows::UI::Xaml::Input::ICommand^ m_navigateLibraryCommand;
        Windows::UI::Xaml::Input::ICommand^ m_navigateSettingsCommand;
    protected:
        void OnPropertyChanged(Platform::String^ propertyName);
    };
}


