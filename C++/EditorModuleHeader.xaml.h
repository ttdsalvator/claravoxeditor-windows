﻿//
// EditorModuleHeader.xaml.h
// Declaration of the EditorModuleHeader class
//

#pragma once

#include "EditorModuleHeader.g.h"

namespace ClaravoxEditor
{
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class EditorModuleHeader sealed
	{
	public:
		EditorModuleHeader();
	};
}
