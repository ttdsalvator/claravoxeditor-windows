#include "pch.h"
#include "ParamBaseViewModel.h"
using namespace ClaravoxEditor;
using namespace Platform;
using namespace Windows::ApplicationModel::Core;
using namespace Windows::UI::Core;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Navigation;

ParamBaseViewModel::ParamBaseViewModel()
{
}
void ParamBaseViewModel::OnPropertyChanged(String^ propertyName)
{
    PropertyChanged(this, ref new PropertyChangedEventArgs(propertyName));
}

String^ ParamBaseViewModel::Name::get()
{
    return _name;
}

void ParamBaseViewModel::Name::set(String^ value)
{
    _name = value;
    OnPropertyChanged("Name");
}