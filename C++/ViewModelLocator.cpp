#include "pch.h"
#include "ViewModelLocator.h"


using namespace ClaravoxEditor;

using namespace Concurrency;
using namespace Platform;
using namespace Platform::Collections;
using namespace Windows::Foundation::Collections;

/// <summary> Create common trip store reference, set up view models.</summary>
ViewModelLocator::ViewModelLocator()
{
    this->modelSet = ref new Map<Platform::String^, BaseViewModel^>();
    this->modelSet->Insert(L"ParametersListVM", ref new ClaravoxEditor::ParametersListVM());
}

ClaravoxEditor::ParametersListVM^ ViewModelLocator::ParametersListVM::get()
{
    return (ClaravoxEditor::ParametersListVM^)this->modelSet->Lookup("ParametersListVM");
}