#include "pch.h"
#include "ParametersListVM.h"
#include <ParamStringViewModel.h>
#include <ParamIntViewModel.h>
#include <ModuleVM.h>
#include <string>

using namespace ClaravoxEditor;
using namespace Platform::Collections;

ParametersListVM::ParametersListVM()
{
    init();
}

void ParametersListVM::init()
{
    //SliderVMList = ref new Vector<ParamBaseViewModel^>();
    ModuleVMList = ref new Vector<ModuleVM^>();

  /*  ParamIntViewModel^ slider1 = ref new ParamIntViewModel();
    slider1->Min = 0;
    slider1->Max = 100;
    slider1->Value = 10;

    SliderVMList->Append(slider1);

    ParamIntViewModel^ slider2 = ref new ParamIntViewModel();
    slider2->Min = 0;
    slider2->Max = 100;
    slider2->Value = 50;

    SliderVMList->Append(slider2);

    ParamStringViewModel^ cbViewModel = ref new ParamStringViewModel();
    cbViewModel->Value = "50";

    SliderVMList->Append(cbViewModel);*/
    size_t cnt = 50;
    for (size_t i = 0; i < cnt; i++)
    {
        ModuleVM^ module = ref new ModuleVM();
        String^ name ="Module " + i.ToString();
        
        module->init(name);
        module->addIntParameter(0, "Name1", 0, 100, 10);
        module->addIntParameter(1, "Name2", 0, 100, 10);

        ModuleVMList->Append(module);
    }
}

//IObservableVector<ParamBaseViewModel^>^ ParametersListVM::SliderVMList::get() 
//{
//    return _sliderVMList;
//}

//void ParametersListVM::SliderVMList::set(IObservableVector<ParamBaseViewModel^>^ value) 
//{
//    _sliderVMList = value;
//    OnPropertyChanged("SliderVMList");
//}

IObservableVector<ModuleVM^>^ ParametersListVM::ModuleVMList::get()
{
    return _modulesVMList;
}


void ParametersListVM::ModuleVMList::set(IObservableVector<ModuleVM^>^ value) 
{
    _modulesVMList = value;
    OnPropertyChanged("ModuleVMList");
}