#pragma once
namespace ClaravoxEditor {
    public ref class BaseViewModel : public Windows::UI::Xaml::DependencyObject, Windows::UI::Xaml::Data::INotifyPropertyChanged
    {
    internal:
        BaseViewModel();

    public:
        // Inherited via INotifyPropertyChanged
        virtual event Windows::UI::Xaml::Data::PropertyChangedEventHandler^ PropertyChanged;

    protected:
        void OnPropertyChanged(Platform::String^ propertyName);
    };
}

