﻿//
// LibraryView.xaml.h
// Declaration of the LibraryView class
//

#pragma once

#include "LibraryView.g.h"

namespace ClaravoxEditor
{
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class LibraryView sealed
	{
	public:
		LibraryView();
	};
}
