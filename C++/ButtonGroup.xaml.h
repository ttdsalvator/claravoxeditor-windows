﻿//
// ButtonGroup.xaml.h
// Declaration of the ButtonGroup class
//

#pragma once

#include "ButtonGroup.g.h"

namespace ClaravoxEditor
{
	/// <summary>
	/// An empty page that can be used on its own or navigated to within a Frame.
	/// </summary>
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class ButtonGroup sealed
	{
	public:
		ButtonGroup();
	};
}
