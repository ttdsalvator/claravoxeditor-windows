﻿//
// MyUserControl.xaml.h
// Declaration of the EditorToolbar class
//

#pragma once

#include "EditorToolbar.g.h"

namespace ClaravoxEditor
{
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class EditorToolbar sealed
	{
	public:
		EditorToolbar();

		void AppBarButtonLibrary_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
		{
			settingsButton->IsChecked = false;			
		}

		void AppBarButtonSettings_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
		{
			libraryButton->IsChecked = false;
		}
	
	private:
	};
}
