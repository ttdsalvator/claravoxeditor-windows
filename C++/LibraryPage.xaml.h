﻿//
// LibraryPage.xaml.h
// Déclaration de la classe LibraryPage
//

#pragma once

#include "LibraryPage.g.h"

namespace ClaravoxEditor
{
	/// <summary>
	/// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
	/// </summary>
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class LibraryPage sealed
	{
	public:
		LibraryPage();
	};
}
