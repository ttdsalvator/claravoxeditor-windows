#pragma once
#include "ParamBaseViewModel.h"

using namespace Platform;
using namespace Windows::UI::Xaml::Data;

namespace ClaravoxEditor {
    [Windows::UI::Xaml::Data::Bindable]
    [Windows::Foundation::Metadata::WebHostHidden]
    public ref class ParamStringViewModel sealed : ParamBaseViewModel
    {
    public:
        ParamStringViewModel();

        property String^ Value
        {
            String^ get();
            void set(String^ value);
        }

    private:
        String^ _value;
    };
}