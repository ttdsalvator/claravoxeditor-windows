﻿//
// Osc1Module.xaml.h
// Declaration of the Osc1Module class
//

#pragma once

#include "Osc1Module.g.h"

namespace ClaravoxEditor
{
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class Osc1Module sealed
	{
	public:
		Osc1Module();
	};
}
