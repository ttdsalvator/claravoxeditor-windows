﻿//
// ParametersView.xaml.h
// Declaration of the ParametersView class
//

#pragma once

#include "ParametersView.g.h"

namespace ClaravoxEditor
{
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class ParametersView sealed
	{
	public:
		ParametersView();
	};
}
