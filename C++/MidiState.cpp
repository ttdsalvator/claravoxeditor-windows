#include "pch.h"
#include "MidiState.h"
using namespace ClaravoxEditor;
using namespace Platform::Collections;

using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Input;

MidiState::MidiState()
{
    m_saveCommand = ref new DelegateCommand(ref new ExecuteDelegate(this, &MidiState::Save), nullptr);
    m_saveAsCommand = ref new DelegateCommand(ref new ExecuteDelegate(this, &MidiState::SaveAs), nullptr);
    m_nextPresetCommand = ref new DelegateCommand(ref new ExecuteDelegate(this, &MidiState::NextPreset), nullptr);
    m_prevPresetCommand = ref new DelegateCommand(ref new ExecuteDelegate(this, &MidiState::PrevPreset), nullptr);
    m_showMoreCommand = ref new DelegateCommand(ref new ExecuteDelegate(this, &MidiState::ShowMore), nullptr);
    m_navigateParametersCommand = ref new DelegateCommand(ref new ExecuteDelegate(this, &MidiState::NavigateToParameters), nullptr);
    m_navigateLibraryCommand = ref new DelegateCommand(ref new ExecuteDelegate(this, &MidiState::NavigateToLibrary), nullptr);
    m_navigateSettingsCommand = ref new DelegateCommand(ref new ExecuteDelegate(this, &MidiState::NavigateToSettings), nullptr);
}

void MidiState::Save(Object^ parameter)
{

}

void MidiState::SaveAs(Object^ parameter)
{

}

void MidiState::NextPreset(Object^ parameter)
{

}

void MidiState::PrevPreset(Object^ parameter)
{

}

void MidiState::ShowMore(Object^ parameter)
{

}

void MidiState::NavigateToParameters(Platform::Object^ parameter) 
{
    NavigateToPage(0, parameter);
}

void MidiState::NavigateToLibrary(Platform::Object^ parameter) 
{
    NavigateToPage(0, parameter);
}

void MidiState::NavigateToSettings(Platform::Object^ parameter) 
{
    NavigateToPage(4, parameter);
}

ICommand^ MidiState::SaveCommand::get()
{
    return m_saveCommand;
}

ICommand^ MidiState::SaveAsCommand::get()
{
    return m_saveAsCommand;
}

ICommand^ MidiState::NextPresetCommand::get()
{
    return m_nextPresetCommand;
}

ICommand^ MidiState::PrevPresetCommand::get()
{
    return m_prevPresetCommand;
}

ICommand^ MidiState::ShowMoreCommand::get()
{
    return m_showMoreCommand;
}

ICommand^ MidiState::NavigateParametersCommand::get()
{
    return m_navigateParametersCommand;
}

ICommand^ MidiState::NavigateLibraryCommand::get()
{
    return m_navigateParametersCommand;
}

ICommand^ MidiState::NavigateSettingsCommand::get()
{
    return m_navigateSettingsCommand;
}

bool MidiState::IsOpen::get()
{
    return _isOpen;
}

void MidiState::IsOpen::set(bool value)
{
    _isOpen = value;
    OnPropertyChanged("IsOpen");
}

double MidiState::Width::get()
{
    return _width;
}

void MidiState::Width::set(double value)
{
    _width = value;
    OnPropertyChanged("Width");
}
double MidiState::ActualWidth::get()
{
    return _actualWidth;
}

void MidiState::ActualWidth::set(double value)
{
    _actualWidth = value;
    _width = _actualWidth * 0.5;
    OnPropertyChanged("ActualWidth");
    OnPropertyChanged("Width");
}

void MidiState::OnPropertyChanged(String^ propertyName)
{
    PropertyChanged(this, ref new PropertyChangedEventArgs(propertyName));
}
