﻿//
// SettingsControl.xaml.h
// Declaration of the SettingsControl class
//

#pragma once

#include "SettingsControl.g.h"

namespace ClaravoxEditor
{
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class SettingsControl sealed
	{
	public:
		SettingsControl();
	};
}
