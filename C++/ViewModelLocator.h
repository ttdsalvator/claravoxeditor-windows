#pragma once
#include <ParametersListVM.h>


namespace ClaravoxEditor
{
    [Windows::UI::Xaml::Data::Bindable]
    [Windows::Foundation::Metadata::WebHostHidden]
    public ref class ViewModelLocator sealed
    {
    public:
        ViewModelLocator();

        property ParametersListVM^ ParametersListVM
        {
            ClaravoxEditor::ParametersListVM^ get();
        }

    private:
        Windows::Foundation::Collections::IMap<Platform::String^, BaseViewModel^>^ modelSet;
    };
}