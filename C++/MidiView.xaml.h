﻿//
// MidiView.xaml.h
// Declaration of the MidiView class
//

#pragma once

#include "MidiView.g.h"

namespace ClaravoxEditor
{
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class MidiView sealed
	{
	public:
		MidiView();
	};
}
