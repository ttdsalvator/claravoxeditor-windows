#pragma once
#include <ParamBaseViewModel.h>
#include<BaseViewModel.h>
#include<ModuleVM.h>
using namespace Platform;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::Foundation::Collections;

namespace ClaravoxEditor 
{
    [Windows::UI::Xaml::Data::Bindable]
    [Windows::Foundation::Metadata::WebHostHidden]
public ref class ParametersListVM sealed : BaseViewModel
{
public:
    ParametersListVM();

    //property IObservableVector<ParamBaseViewModel^>^ SliderVMList
    //{
    //    IObservableVector<ParamBaseViewModel^>^ get();
    //    void set(IObservableVector<ParamBaseViewModel^>^ value);
    //};

    property IObservableVector<ModuleVM^>^ ModuleVMList
    {
        IObservableVector<ModuleVM^>^ get();
        void set(IObservableVector<ModuleVM^>^ value);
    };

private:
    void init();

    //IObservableVector<ParamBaseViewModel^>^ _sliderVMList;
    IObservableVector<ModuleVM^>^ _modulesVMList;
};
}


