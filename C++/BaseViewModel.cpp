#include "pch.h"
#include "BaseViewModel.h"

using namespace ClaravoxEditor;
using namespace Platform;
using namespace Windows::ApplicationModel::Core;
using namespace Windows::UI::Core;
using namespace Windows::UI::Xaml::Data;

BaseViewModel::BaseViewModel()
{
}

void BaseViewModel::OnPropertyChanged(String^ propertyName)
{
	PropertyChanged(this, ref new PropertyChangedEventArgs(propertyName));
}