﻿//
// TimbreModule.xaml.h
// Declaration of the TimbreModule class
//

#pragma once

#include "TimbreModule.g.h"

namespace ClaravoxEditor
{
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class TimbreModule sealed
	{
	public:
		TimbreModule();
	};
}
