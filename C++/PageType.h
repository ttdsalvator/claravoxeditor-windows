#pragma once

namespace ClaravoxEditor {
    enum Panels {
        panelParameters = 0,
        panelMidi,
        panelLibrary,
        panelCompactFocus,
        panelSettings,
        panelService,
    };
}
