#pragma once
#include <ModulesListVM.h>


namespace ClaravoxEditor
{
    [Windows::UI::Xaml::Data::Bindable]
    [Windows::Foundation::Metadata::WebHostHidden]
    public ref class ViewModelLocator sealed
    {
    public:
        ViewModelLocator();

        property ModulesListVM^ ModulesListVM
        {
            ClaravoxEditor::ModulesListVM^ get();
        }

    private:
        Windows::Foundation::Collections::IMap<Platform::String^, BaseViewModel^>^ modelSet;
    };
}