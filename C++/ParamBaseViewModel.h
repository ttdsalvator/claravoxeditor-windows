#pragma once

#include "PageType.h"

namespace ClaravoxEditor
{
	public ref class ParamBaseViewModel : public Windows::UI::Xaml::DependencyObject, Windows::UI::Xaml::Data::INotifyPropertyChanged
    {
    internal:
        ParamBaseViewModel();
    public:
        // Inherited via INotifyPropertyChanged
        virtual event Windows::UI::Xaml::Data::PropertyChangedEventHandler^ PropertyChanged;

        property Platform::String^ Name
        {
            Platform::String^ get();
            void set(Platform::String^ value);
        }

    private:
        Platform::String^ _name;
    protected:
        void OnPropertyChanged(Platform::String^ propertyName);
    };
}

