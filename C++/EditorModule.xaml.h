﻿//
// EditorModule.xaml.h
// Declaration of the EditorModule class
//

#pragma once

#include "EditorModule.g.h"

namespace ClaravoxEditor
{
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class EditorModule sealed
	{
	public:
		EditorModule();
	};
}
