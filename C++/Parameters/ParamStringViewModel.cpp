#include "pch.h"
#include "ParamStringViewModel.h"
using namespace ClaravoxEditor;

ParamStringViewModel::ParamStringViewModel()
{
}

String^ ParamStringViewModel::Value::get()
{
    return _value;
}

void ParamStringViewModel::Value::set(String^ value)
{
    _value = value;
    OnPropertyChanged("Value");
}


