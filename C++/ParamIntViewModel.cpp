#include "pch.h"
#include "ParamIntViewModel.h"
using namespace ClaravoxEditor;

ParamIntViewModel::ParamIntViewModel()
{
}

int ParamIntViewModel::Min::get()
{
    return _min;
}

void ParamIntViewModel::Min::set(int value)
{
    _min = value;
    OnPropertyChanged("Min");
}

int ParamIntViewModel::Max::get()
{
    return _max;
}

void ParamIntViewModel::Max::set(int value)
{
    _max = value;
    OnPropertyChanged("Max");
}

int ParamIntViewModel::Value::get()
{
    return _value;
}

void ParamIntViewModel::Value::set(int value)
{
    _value = value;
    OnPropertyChanged("Value");
}

