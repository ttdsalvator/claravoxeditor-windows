#pragma once
inline void setPresetParam(double newValue, double& parameter)
{
    parameter = newValue;
}

struct Parameters
{
    double octaveState{ 0.0 };
    double volCurveVal{ 0.0 };
    double filterVal{ 0.0 };
    double waveVal{ 0.0 };
    double brightVal{ 0.0 };
    double quantizeAmount{ 0.0 };
    double rootVal{ 0.0 };
    double scaleVal{ 0.0 };
    double delayAmount{ 0.0 };
    double timeVal{ 0.0 };
    double feedbackVal{ 0.0 };
    double pitchCurveVal{ 0.0 };
    double osc1Type{ 0.0 };
    double osc1Wavetable{ 0.0 };
    double osc1ScanFreq{ 0.0 };
    double osc1ScanPos{ 0.0 };
    double osc1ScanAmount{ 0.0 };
    double osc1Level{ 0.0 };
    double osc2Type{ 0.0 };
    double osc2Wavetable{ 0.0 };
    double osc2ScanFreq{ 0.0 };
    double osc2ScanPos{ 0.0 };
    double osc2ScanAmount{ 0.0 };
    double osc2Level{ 0.0 };
    double osc2Freq{ 0.0 };
    double noiseLevel{ 0.0 };
    //    double pitch_track_amt{ 0.0 };
    //    double pitch_track_dest{ 0.0 };
    //    double volume_track_amt{ 0.0 };
    //    double volume_track_dest{ 0.0 };
    //    double cv_in_assign{ 0.0 };
    double cvInAmount{ 0.0 };
    //    double cv_in_offset{ 0.0 };
    double cvInScale{ 0.0 };
    //    double pitchCVMode{ 0.0 };
    //    double pitchCVSource{ 0.0 };
    //    double pitchCVCorrect{ 0.0 };
    double pitchModWsFreq{ 0.0 };
    double pitchModWsAmount{ 0.0 };
    double osc2FilterMode{ 0.0 };
    double osc2FilterFreq{ 0.0 };
    double osc2FilterRes{ 0.0 };
    double osc1FilterPitchAmount{ 0.0 };
    double osc2FilterPitchAmount{ 0.0 };
    double osc1FilterVolumeAmount{ 0.0 };
    double osc2FilterVolumeAmount{ 0.0 };
};