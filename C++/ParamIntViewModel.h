#pragma once

#include "ParamBaseViewModel.h"

using namespace Platform;
using namespace Windows::UI::Xaml::Data;

namespace ClaravoxEditor {
    [Windows::UI::Xaml::Data::Bindable]
    [Windows::Foundation::Metadata::WebHostHidden]
    public ref class  ParamIntViewModel sealed : public ParamBaseViewModel
    {
    public:
        ParamIntViewModel();

        property int Min
        {
            int get();
            void set(int value);
        }

        property int Max
        {
            int get();
            void set(int value);
        }

        property int Value
        {
            int get();
            void set(int value);
        }

    private:
        int _min;
        int _max;
        int _value;
    };
}

