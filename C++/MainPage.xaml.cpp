﻿//
// MainPage.xaml.cpp
// Implémentation de la classe MainPage.
//

#include "pch.h"
#include "MainPage.xaml.h"

using namespace ClaravoxEditor;

using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;

// Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409


MainPage::MainPage()
{
	InitializeComponent();	
	MainPage::midiState = ref new MidiState();
	MainPage::midiState->NavigateToPage::add(ref new PageNavigateEventHandler(this, &MainPage::OnNavigateToPage));
	DataContext = midiState;
}

void ClaravoxEditor::MainPage::btSettings_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	MainPage::midiState->IsOpen = !MainPage::midiState->IsOpen;
}

void ClaravoxEditor::MainPage::OnNavigateToPage(int page, Platform::Object^ parameter)
{
	showPanel(static_cast<Panels>(page));
}
