﻿//
// MyUserControl.xaml.h
// Declaration of the MyUserControl class
//

#pragma once

#include "MyUserControl.g.h"

namespace ClaravoxEditor
{
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class MyUserControl sealed
	{
	public:
		MyUserControl();
	};
}
