﻿//
// SettingsPage.xaml.h
// Déclaration de la classe SettingsPage
//

#pragma once

#include "SettingsPage.g.h"

namespace ClaravoxEditor
{
	/// <summary>
	/// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
	/// </summary>
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class SettingsPage sealed
	{
	public:
		SettingsPage();
	};
}
