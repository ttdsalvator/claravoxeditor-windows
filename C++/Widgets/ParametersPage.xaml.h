﻿//
// ParametersPage.xaml.h
// Déclaration de la classe ParametersPage
//

#pragma once

#include "ParametersPage.g.h"

namespace ClaravoxEditor
{
	/// <summary>
	/// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
	/// </summary>
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class ParametersPage sealed
	{
	public:
		ParametersPage();
	};
}
