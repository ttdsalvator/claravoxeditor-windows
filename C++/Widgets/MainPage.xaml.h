﻿//
// MainPage.xaml.h
// Déclaration de la classe MainPage.
//

#pragma once

#include "MainViewModel.h"
#include "MainPage.g.h"
#include <SettingsPage.xaml.h>
#include <ParametersPage.xaml.h>

namespace ClaravoxEditor
{
    enum  Panels{
        panelParameters = 0,
        panelMidi,
        panelLibrary,
        panelCompactFocus,
        panelSettings,
        panelService,
    };

	/// <summary>
	/// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
	/// </summary>
       
      public ref class MainPage sealed
	{
	public:
		MainPage();
       
         // = ref new MainViewModel();
   
        void button1Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
        {
            showPanel(panelParameters);
        }
        
        void button2Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
        {
            showPanel(panelSettings);
        }

    private:
        void showPanel(Panels panel)
        {
            switch (panel)
            {
            case ClaravoxEditor::panelParameters:
                this->frame->Navigate(ParametersPage::typeid);
                break;
            case ClaravoxEditor::panelMidi:
                this->frame->Navigate(ParametersPage::typeid);
                break;
            case ClaravoxEditor::panelLibrary:
                this->frame->Navigate(ParametersPage::typeid);
                break;
            case ClaravoxEditor::panelCompactFocus:
                this->frame->Navigate(ParametersPage::typeid);
                break;
            case ClaravoxEditor::panelSettings:
                this->frame->Navigate(SettingsPage::typeid);
                break;
            case ClaravoxEditor::panelService:
                this->frame->Navigate(ParametersPage::typeid);
                break;
            default:
                this->frame->Navigate(ParametersPage::typeid);
                break;
            }
        }
	};
}
