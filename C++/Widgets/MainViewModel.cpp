#include "pch.h"
#include "MainViewModel.h"

using namespace ClaravoxEditor;

MainViewModel::MainViewModel()
{
}

int MainViewModel::Min::get()
{
    return _min;
}

void MainViewModel::Min::set(int value)
{
    _min = value;
    OnPropertyChanged("Min");
}

int MainViewModel::Max::get()
{
    return _max;
}

void MainViewModel::Max::set(int value)
{
    _max = value;
    OnPropertyChanged("Max");
}
int MainViewModel::Value::get()
{
    return _value;
}
void MainViewModel::Value::set(int value)
{
    _value = value;
    OnPropertyChanged("Value");
}
void MainViewModel::OnPropertyChanged(String^ propertyName)
{
    PropertyChanged(this, ref new PropertyChangedEventArgs(propertyName));
}