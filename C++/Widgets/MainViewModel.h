// "MainViewModel.h"

#pragma once

using namespace Platform;
using namespace Windows::UI::Xaml::Data;

namespace ClaravoxEditor {
    [Windows::UI::Xaml::Data::Bindable]
    [Windows::Foundation::Metadata::WebHostHidden]
	public ref class  MainViewModel sealed : Windows::UI::Xaml::Data::INotifyPropertyChanged
	{
    public:
        MainViewModel();

        virtual event Windows::UI::Xaml::Data::PropertyChangedEventHandler^ PropertyChanged;

        property int Min
        {
            int get();
            void set(int value);
        }

        property int Max
        {
            int get();
            void set(int value);
        }
        property int Value
        {
            int get();
            void set(int value);
        }
    private:
        int _min;
        int _max;
        int _value;
    protected:
        void OnPropertyChanged(Platform::String^ propertyName);
	};
}


