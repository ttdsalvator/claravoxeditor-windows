﻿//
// EditorNavigationBar.xaml.h
// Declaration of the EditorNavigationBar class
//

#pragma once

#include "EditorNavigationBar.g.h"

namespace ClaravoxEditor
{
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class EditorNavigationBar sealed
	{
	public:
		EditorNavigationBar();
	};
}
