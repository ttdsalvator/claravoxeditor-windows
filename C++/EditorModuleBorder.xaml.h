﻿//
// EditorModuleBorder.xaml.h
// Declaration of the EditorModuleBorder class
//

#pragma once

#include "EditorModuleBorder.g.h"

namespace ClaravoxEditor
{
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class EditorModuleBorder sealed
	{
	public:
		EditorModuleBorder();
	};
}
