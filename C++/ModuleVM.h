#pragma once
#include <ParamBaseViewModel.h>
#include<BaseViewModel.h>
#include <ButtonVM.h>
using namespace Platform;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::Foundation::Collections;

namespace ClaravoxEditor {
    [Windows::UI::Xaml::Data::Bindable]
    [Windows::Foundation::Metadata::WebHostHidden]
    public ref class ModuleVM sealed : BaseViewModel
    {
    public:
        ModuleVM();

        void init(String^ title);
        void addIntParameter(int index, String^ name, int min, int max, int defaultVal);

        property String^ Title
        {
            String^ get();
            void set(String^ value);
        }
        property String^ ParamName
        {
            String^ get();
            void set(String^ value);
        }
        property IObservableVector<ParamBaseViewModel^>^ ParamsVMList
        {
            IObservableVector<ParamBaseViewModel^>^ get();
            void set(IObservableVector<ParamBaseViewModel^>^ value);
        };
        property IObservableVector<ButtonVM^>^ ButtonVMList
        {
            IObservableVector<ButtonVM^>^ get();
            void set(IObservableVector<ButtonVM^>^ value);
        };

    private:
        String^ _value;
        String^ _paramName;
        IObservableVector<ButtonVM^>^ _buttonVMList;
        IObservableVector<ParamBaseViewModel^>^ _params;
    };
}
