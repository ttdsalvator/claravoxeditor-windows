//*********************************************************
//
// Copyright (c) Microsoft. All rights reserved.
// This code is licensed under the MIT License (MIT).
// THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
// IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
// PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
//
//*********************************************************

//using SDKTemplate;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Windows.Devices.Enumeration;
using Windows.Devices.Midi;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

using Claravox.MIDI;

namespace Claravox
{
    /// <summary>
    /// DeviceWatcher class to monitor adding/removing MIDI devices on the fly
    /// </summary>
    public class MidiDeviceWatcher: Page
    {
        // ------------ variables ------------
        internal DeviceWatcher inputDeviceWatcher = null;
        internal DeviceWatcher outputDeviceWatcher = null;
        internal DeviceInformationCollection inputDeviceInformationCollection = null;
        internal DeviceInformationCollection outputDeviceInformationCollection = null;
        bool inputEnumerationCompleted = false;
        bool outputEnumerationCompleted = false;
        string inputMidiSelector = string.Empty;
        string outputMidiSelector = string.Empty;
        List<String> inputPortList_ = new List<String>();
        List<String> outputPortList_ = new List<String>();

        MidiManager midiManagerPtr_;

        // ------------ methods------------ 
        /// <summary>
        /// Constructor: Initialize and hook up Device Watcher events
        /// </summary>
        /// <param name="midiSelectorString">MIDI Device Selector</param>
        internal MidiDeviceWatcher(string inputMidiSelectorString, string outputMidiSelectorString, MidiManager midiManager)
        {
            this.inputDeviceWatcher = DeviceInformation.CreateWatcher(inputMidiSelectorString);
            this.inputMidiSelector = inputMidiSelectorString;

            this.outputDeviceWatcher = DeviceInformation.CreateWatcher(outputMidiSelectorString);
            this.outputMidiSelector = outputMidiSelectorString;
           
            this.midiManagerPtr_ = midiManager;

            // callback
            this.inputDeviceWatcher.Added += InputDeviceWatcher_Added;
            this.inputDeviceWatcher.Removed += InputDeviceWatcher_Removed;
            this.inputDeviceWatcher.Updated += InputDeviceWatcher_Updated;
            this.inputDeviceWatcher.EnumerationCompleted += InputDeviceWatcher_EnumerationCompleted;
            
            this.outputDeviceWatcher.Added += OutputDeviceWatcher_Added;
            this.outputDeviceWatcher.Removed += OutputDeviceWatcher_Removed;
            this.outputDeviceWatcher.Updated += OutputDeviceWatcher_Updated;
            this.outputDeviceWatcher.EnumerationCompleted += OutputDeviceWatcher_EnumerationCompleted;
        }

        /// <summary>
        /// Destructor: Remove Device Watcher events
        /// </summary>
        ~MidiDeviceWatcher()
        {
            this.inputDeviceWatcher.Added -= InputDeviceWatcher_Added;
            this.inputDeviceWatcher.Removed -= InputDeviceWatcher_Removed;
            this.inputDeviceWatcher.Updated -= InputDeviceWatcher_Updated;
            this.inputDeviceWatcher.EnumerationCompleted -= InputDeviceWatcher_EnumerationCompleted;
            
            this.outputDeviceWatcher.Added -= OutputDeviceWatcher_Added;
            this.outputDeviceWatcher.Removed -= OutputDeviceWatcher_Removed;
            this.outputDeviceWatcher.Updated -= OutputDeviceWatcher_Updated;
            this.outputDeviceWatcher.EnumerationCompleted -= OutputDeviceWatcher_EnumerationCompleted;
        }

        /// <summary>
        /// Start the Device Watcher
        /// </summary>
        internal void Start()
        {
            if(this.inputDeviceWatcher.Status != DeviceWatcherStatus.Started)
            {
                this.inputDeviceWatcher.Start();
            }

            if(this.outputDeviceWatcher.Status != DeviceWatcherStatus.Started)
            {
                this.outputDeviceWatcher.Start();
            }
        }

        /// <summary>
        /// Stop the Device Watcher
        /// </summary>
        internal void Stop()
        {
            if(this.inputDeviceWatcher.Status != DeviceWatcherStatus.Stopped)
            {
                this.inputDeviceWatcher.Stop();
            }

            if (this.outputDeviceWatcher.Status != DeviceWatcherStatus.Stopped)
            {
                this.outputDeviceWatcher.Stop();
            }
        }

        /// <summary>
        /// Get the DeviceInformationCollection
        /// </summary>
        /// <returns></returns>
        internal DeviceInformationCollection GetInputDeviceInformationCollection()
        {
            return this.inputDeviceInformationCollection;
        }
       
        internal DeviceInformationCollection GetOutputDeviceInformationCollection()
        {
            return this.outputDeviceInformationCollection;
        }

        /// <summary>
        /// Add any connected MIDI devices to the list
        /// </summary>
        private async void UpdateInputDevicesList()
        {
            // Get a list of all MIDI devices
            this.inputDeviceInformationCollection = await DeviceInformation.FindAllAsync(this.inputMidiSelector);
            this.outputDeviceInformationCollection = await DeviceInformation.FindAllAsync(this.outputMidiSelector);

            // If no devices are found, update the ListBox
            if ((this.inputDeviceInformationCollection == null) || (this.inputDeviceInformationCollection.Count == 0))
            {
                // Start with a clean list
                this.inputPortList_.Clear();
            }
            // If devices are found, enumerate them and add them to the list
            else
            {
                // Start with a clean list
                this.inputPortList_.Clear();

                foreach (var device in inputDeviceInformationCollection)
                {
                    this.inputPortList_.Add(device.Name);
                }
            }

            if (midiManagerPtr_ != null)
            {
                midiManagerPtr_.inputDevicesListChanged();
            }
        }

        private async void UpdateOutputDevicesList()
        {
            // Get a list of all MIDI devices
            this.outputDeviceInformationCollection = await DeviceInformation.FindAllAsync(this.outputMidiSelector);

            // If no devices are found, update the ListBox
            if ((this.outputDeviceInformationCollection == null) || (this.outputDeviceInformationCollection.Count == 0))
            {
                // Start with a clean list
                this.outputPortList_.Clear();
            }
            // If devices are found, enumerate them and add them to the list
            else
            {
                // Start with a clean list
                this.outputPortList_.Clear();

                foreach (var device in outputDeviceInformationCollection)
                {
                    this.outputPortList_.Add(device.Name);
                }
            }

            if (midiManagerPtr_ != null)
            {
                midiManagerPtr_.outputDevicesListChanged();
            }
        }

        /// <summary>
        /// Retreive the list of found ports
        /// </summary>
        public List<String> getInputPortsList()
        {
            return inputPortList_;
        }

        public List<String> getOutputPortsList()
        {
            return outputPortList_;
        }
        /// <summary>
        /// Update UI on device added
        /// </summary>
        /// <param name="sender">The active DeviceWatcher instance</param>
        /// <param name="args">Event arguments</param>
        private async void InputDeviceWatcher_Added(DeviceWatcher sender, DeviceInformation args)
        {
            // If all devices have been enumerated
            if (this.inputEnumerationCompleted)
            {
                await Dispatcher.RunAsync(CoreDispatcherPriority.High, () =>
                {
                    // Update the device list
                    UpdateInputDevicesList();
                });
            }
        }

        private async void OutputDeviceWatcher_Added(DeviceWatcher sender, DeviceInformation args)
        {
            // If all devices have been enumerated
            if (this.outputEnumerationCompleted)
            {
                await Dispatcher.RunAsync(CoreDispatcherPriority.High, () =>
                {
                    // Update the device list
                    UpdateOutputDevicesList();
                });
            }
        }
        /// <summary>
        /// Update UI on device removed
        /// </summary>
        /// <param name="sender">The active DeviceWatcher instance</param>
        /// <param name="args">Event arguments</param>
        private async void InputDeviceWatcher_Removed(DeviceWatcher sender, DeviceInformationUpdate args)
        {
            // If all devices have been enumerated
            if (this.inputEnumerationCompleted)
            {
                await Dispatcher.RunAsync(CoreDispatcherPriority.High, () =>
                {
                    // Update the device list
                    UpdateInputDevicesList();
                });
            }
        }

        private async void OutputDeviceWatcher_Removed(DeviceWatcher sender, DeviceInformationUpdate args)
        {
            // If all devices have been enumerated
            if (this.outputEnumerationCompleted)
            {
                await Dispatcher.RunAsync(CoreDispatcherPriority.High, () =>
                {
                    // Update the device list
                    UpdateOutputDevicesList();
                });
            }
        }

        /// <summary>
        /// Update UI on device updated
        /// </summary>
        /// <param name="sender">The active DeviceWatcher instance</param>
        /// <param name="args">Event arguments</param>
        private async void InputDeviceWatcher_Updated(DeviceWatcher sender, DeviceInformationUpdate args)
        {
            // If all devices have been enumerated
            if (this.inputEnumerationCompleted)
            {
                await Dispatcher.RunAsync(CoreDispatcherPriority.High, () =>
                {
                    // Update the device list
                    UpdateInputDevicesList();
                });
            }
        }

        private async void OutputDeviceWatcher_Updated(DeviceWatcher sender, DeviceInformationUpdate args)
        {
            // If all devices have been enumerated
            if (this.outputEnumerationCompleted)
            {
                await Dispatcher.RunAsync(CoreDispatcherPriority.High, () =>
                {
                    // Update the device list
                    UpdateOutputDevicesList();
                });
            }
        }
        /// <summary>
        /// Update UI on device enumeration completed.
        /// </summary>
        /// <param name="sender">The active DeviceWatcher instance</param>
        /// <param name="args">Event arguments</param>
        private async void InputDeviceWatcher_EnumerationCompleted(DeviceWatcher sender, object args)
        {
            this.inputEnumerationCompleted = true;
            await Dispatcher.RunAsync(CoreDispatcherPriority.High, () =>
            {
                // Update the device list
                UpdateInputDevicesList();
            });
        }

        private async void OutputDeviceWatcher_EnumerationCompleted(DeviceWatcher sender, object args)
        {
            this.outputEnumerationCompleted = true;
            await Dispatcher.RunAsync(CoreDispatcherPriority.High, () =>
            {
                // Update the device list
                UpdateOutputDevicesList();
            });
        }
    }
}
