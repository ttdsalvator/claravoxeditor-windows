﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Enumeration;
using Windows.Devices.Midi;
using Windows.Storage.Streams;
using Windows.UI.Core;
using Windows.UI.Xaml.Controls;

using Claravox.ViewModels;
using Claravox.Models;
using System.Collections.ObjectModel;

namespace Claravox.MIDI
{
    public class MidiManager : BindableBase
    {

        /// <summary>
        // -------------------VARIABLES-----------------
        /// </summary>
        private MidiState midiStatePtr_;

        /// Device watcher for MIDI ports
        public MidiDeviceWatcher midiDevicesWatcher;

        /// Collection of active Midi Ports
        public List<MidiInPort> midiInPorts_ = new List<MidiInPort>();
        public List<IMidiOutPort> midiOutPorts_ = new List<IMidiOutPort>();

        public List<String> inputNamesArray = new List<String>();
        public List<String> outputNamesArray = new List<String>();

        /// <summary>
        /// Keep track of the current output device (which could also be the GS synth)
        /// </summary>
        IMidiOutPort currentMidiOutputDevice;


        /// <summary>
        // -------------------PROPERTIES-----------------
        /// </summary>        
        public String selectedInputName_ = String.Empty;
        public string InputName
        {
            get { return selectedInputName_; }
            set
            {
                if (selectedInputName_ != value)
                {
                    selectedInputName_ = value;
                    OnPropertyChanged();
                }
            }
        }
        public String selectedOutputName_ = String.Empty;
        public string OutputName
        {
            get { return selectedOutputName_; }
            set
            {
                if (selectedOutputName_ != value)
                {
                    selectedOutputName_ = value;
                    OnPropertyChanged();
                }
            }
        }
        public int appMidiChannel_ = 0;
        public int midiInputChannel_ = 0;
        public int midiOutputChannel_ = 0;
        public bool is14Bits_ = false;

        public List<String> InputList
        {
            get { return inputNamesArray; }
            set
            {
                inputNamesArray = value;
                OnPropertyChanged();
            }
        }

        public List<String> OutputList
        {
            get { return outputNamesArray; }
            set
            {
                outputNamesArray = value;
                OnPropertyChanged();
            }
        }

        public int AppMidiChannel
        {
            get { return appMidiChannel_; }
            set
            {
                appMidiChannel_ = value;
                OnPropertyChanged();
            }
        }

        public int MidiInputChannel
        {
            get { return midiInputChannel_; }
            set
            {
                midiInputChannel_ = value;
                OnPropertyChanged();
            }
        }

        public int MidiOutputChannel
        {
            get { return midiOutputChannel_; }
            set
            {
                midiOutputChannel_ = value;
                OnPropertyChanged();
            }
        }

        public bool Is14Bits
        {
            get { return is14Bits_; }
            set
            {
                is14Bits_ = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        // -------------------METHODS-----------------
        /// </summary>
        public MidiManager(MidiState midiState)
        {
            midiStatePtr_ = midiState;

            // Set up the MIDI input device watcher
            midiDevicesWatcher = new MidiDeviceWatcher(MidiInPort.GetDeviceSelector(), MidiOutPort.GetDeviceSelector(), this);

            // Start watching for devices
            midiDevicesWatcher.Start();
           // App.Current.get
        }

        ~MidiManager()
        {

            // Stop the devices watcher
            this.midiDevicesWatcher.Stop();

            // Close all MidiInPorts
            foreach (MidiInPort inPort in this.midiInPorts_)
            {
                inPort.Dispose();
            }
            this.midiInPorts_.Clear();

            // Close all MidiOutPorts
            this.midiOutPorts_.Clear();
        }

        // Multi-view app should use the dispatcher for the UI thread they want to run on.
        private void receivedCC(int channel, int number, int value)
        {
            midiStatePtr_.receivedCC(number, value, channel);
        }

        public void sendCC(int channel, int cc, int value)
        {
            IMidiMessage midiMessageToSend = new MidiControlChangeMessage(Convert.ToByte(channel), Convert.ToByte(cc), Convert.ToByte(value));

            if (this.currentMidiOutputDevice != null)
            {
                this.currentMidiOutputDevice.SendMessage(midiMessageToSend);
            }
        }

        public async void HandleMidiMessage(MidiInPort sender, MidiMessageReceivedEventArgs args)
        {
            IMidiMessage receivedMidiMessage = args.Message;

            // Build the received MIDI message into a readable format
            StringBuilder outputMessage = new StringBuilder();
            outputMessage.Append(receivedMidiMessage.Timestamp.ToString()).Append(", Type: ").Append(receivedMidiMessage.Type);

            // Add MIDI message parameters to the output, depending on the type of message
            switch (receivedMidiMessage.Type)
            {
                case MidiMessageType.NoteOff:
                    var noteOffMessage = (MidiNoteOffMessage)receivedMidiMessage;
                    outputMessage.Append(", Channel: ").Append(noteOffMessage.Channel).Append(", Note: ").Append(noteOffMessage.Note).Append(", Velocity: ").Append(noteOffMessage.Velocity);
                    break;
                case MidiMessageType.NoteOn:
                    var noteOnMessage = (MidiNoteOnMessage)receivedMidiMessage;
                    outputMessage.Append(", Channel: ").Append(noteOnMessage.Channel).Append(", Note: ").Append(noteOnMessage.Note).Append(", Velocity: ").Append(noteOnMessage.Velocity);
                    break;
                case MidiMessageType.PolyphonicKeyPressure:
                    var polyphonicKeyPressureMessage = (MidiPolyphonicKeyPressureMessage)receivedMidiMessage;
                    outputMessage.Append(", Channel: ").Append(polyphonicKeyPressureMessage.Channel).Append(", Note: ").Append(polyphonicKeyPressureMessage.Note).Append(", Pressure: ").Append(polyphonicKeyPressureMessage.Pressure);
                    break;
                case MidiMessageType.ControlChange:
                    var controlChangeMessage = (MidiControlChangeMessage)receivedMidiMessage;
                    receivedCC(controlChangeMessage.Channel, controlChangeMessage.Controller, controlChangeMessage.ControlValue);
                    outputMessage.Append(", Channel: ").Append(controlChangeMessage.Channel).Append(", Controller: ").Append(controlChangeMessage.Controller).Append(", Value: ").Append(controlChangeMessage.ControlValue);
                    break;
                case MidiMessageType.ProgramChange:
                    var programChangeMessage = (MidiProgramChangeMessage)receivedMidiMessage;
                    outputMessage.Append(", Channel: ").Append(programChangeMessage.Channel).Append(", Program: ").Append(programChangeMessage.Program);
                    break;
                case MidiMessageType.ChannelPressure:
                    var channelPressureMessage = (MidiChannelPressureMessage)receivedMidiMessage;
                    outputMessage.Append(", Channel: ").Append(channelPressureMessage.Channel).Append(", Pressure: ").Append(channelPressureMessage.Pressure);
                    break;
                case MidiMessageType.PitchBendChange:
                    var pitchBendChangeMessage = (MidiPitchBendChangeMessage)receivedMidiMessage;
                    outputMessage.Append(", Channel: ").Append(pitchBendChangeMessage.Channel).Append(", Bend: ").Append(pitchBendChangeMessage.Bend);
                    break;
                case MidiMessageType.SystemExclusive:
                    var systemExclusiveMessage = (MidiSystemExclusiveMessage)receivedMidiMessage;
                    outputMessage.Append(", ");

                    // Read the SysEx bufffer
                    var sysExDataReader = DataReader.FromBuffer(systemExclusiveMessage.RawData);
                    while (sysExDataReader.UnconsumedBufferLength > 0)
                    {
                        byte byteRead = sysExDataReader.ReadByte();
                        // Pad with leading zero if necessary
                        outputMessage.Append(byteRead.ToString("X2")).Append(" ");
                    }
                    break;
                case MidiMessageType.MidiTimeCode:
                    var timeCodeMessage = (MidiTimeCodeMessage)receivedMidiMessage;
                    outputMessage.Append(", FrameType: ").Append(timeCodeMessage.FrameType).Append(", Values: ").Append(timeCodeMessage.Values);
                    break;
                case MidiMessageType.SongPositionPointer:
                    var songPositionPointerMessage = (MidiSongPositionPointerMessage)receivedMidiMessage;
                    outputMessage.Append(", Beats: ").Append(songPositionPointerMessage.Beats);
                    break;
                case MidiMessageType.SongSelect:
                    var songSelectMessage = (MidiSongSelectMessage)receivedMidiMessage;
                    outputMessage.Append(", Song: ").Append(songSelectMessage.Song);
                    break;
                case MidiMessageType.TuneRequest:
                    var tuneRequestMessage = (MidiTuneRequestMessage)receivedMidiMessage;
                    break;
                case MidiMessageType.TimingClock:
                    var timingClockMessage = (MidiTimingClockMessage)receivedMidiMessage;
                    break;
                case MidiMessageType.Start:
                    var startMessage = (MidiStartMessage)receivedMidiMessage;
                    break;
                case MidiMessageType.Continue:
                    var continueMessage = (MidiContinueMessage)receivedMidiMessage;
                    break;
                case MidiMessageType.Stop:
                    var stopMessage = (MidiStopMessage)receivedMidiMessage;
                    break;
                case MidiMessageType.ActiveSensing:
                    var activeSensingMessage = (MidiActiveSensingMessage)receivedMidiMessage;
                    break;
                case MidiMessageType.SystemReset:
                    var systemResetMessage = (MidiSystemResetMessage)receivedMidiMessage;
                    break;
                case MidiMessageType.None:
                    throw new InvalidOperationException();
                default:
                    break;
            }
        }

        public void setInputPort(String deviceName, MidiInPort currentMidiInputDevice)
        {
            foreach (MidiInPort input in midiInPorts_)
            {
                input.Dispose();
            }

            // We have successfully created a MidiInPort; add the device to the list of active devices, and set up message receiving
            if (!midiInPorts_.Contains(currentMidiInputDevice))
            {
                midiInPorts_.Add(currentMidiInputDevice);
                currentMidiInputDevice.MessageReceived += HandleMidiMessage;
                InputName = deviceName;
            }
        }

        public void setOutputPort(String deviceName, IMidiOutPort device)
        {
            // We have successfully created a MidiInPort; add the device to the list of active devices, and set up message receiving
            if (!midiOutPorts_.Contains(device))
            {
                currentMidiOutputDevice = device;
                midiOutPorts_.Add(device);
                OutputName = deviceName;
            }
        }

        public async void openInputDevice(int index)
        {
            DeviceInformationCollection devInfoCollection = midiDevicesWatcher.GetInputDeviceInformationCollection();
            if (index < 0 || devInfoCollection == null )
            {
                return;
            }
 
            DeviceInformation devInfo = devInfoCollection[index];
            if (devInfo == null)
            {
                return;
            }

            var device = await MidiInPort.FromIdAsync(devInfo.Id);
            if (device == null)
            {
                return;
            }

            // We have successfully created a MidiInPort; add the device to the list of active devices, and set up message receiving
            foreach (MidiInPort input in midiInPorts_)
            {
                input.Dispose();
            }

            if (!midiInPorts_.Contains(device))
            {
                midiInPorts_.Add(device);
            }

            device.MessageReceived += HandleMidiMessage;
            InputName = devInfo.Name;
        }

        public async void openOutputDevice(int index)
        {
            DeviceInformationCollection devInfoCollection = midiDevicesWatcher.GetOutputDeviceInformationCollection();
            if (index < 0 || devInfoCollection == null)
            {
                return;
            }

            DeviceInformation devInfo = devInfoCollection[index];
            if (devInfo == null)
            {
                return;
            }

            var device = await MidiOutPort.FromIdAsync(devInfo.Id);
            if (device == null)
            {
                return;
            }

            // We have successfully created a MidiInPort; add the device to the list of active devices
            if (!midiOutPorts_.Contains(device))
            {
                midiOutPorts_.Add(device);
            }
           
            currentMidiOutputDevice = device;
            OutputName = devInfo.Name;
        }

        public List<String> getInputPorts()
        {
            return midiDevicesWatcher.getInputPortsList();
        }

        public List<String> getOutputPorts()
        {
            return midiDevicesWatcher.getOutputPortsList();
        }

        public void inputDevicesListChanged()
        {
            // copy to observable String array (for SettingsView)
            List<String> in_devices = getInputPorts();
            InputList = in_devices;
            // disable missing input port
            reconnectInput();
            
            if (selectedInputName_ != String.Empty && !in_devices.Contains(selectedInputName_))
            {
                InputName = String.Empty;
 
                foreach (MidiInPort input in midiInPorts_)
                {
                    input.Dispose();
                }
            }         
        }

        public void outputDevicesListChanged()
        {
            // copy to observable String array (for SettingsView)
            List<String> out_devices = getOutputPorts();
            OutputList = out_devices;

            // disable missing output port
            reconnectOutput();
           
            if (selectedOutputName_ != String.Empty && !out_devices.Contains(selectedOutputName_))
            {
                OutputName = String.Empty;
                currentMidiOutputDevice = null;
            }
        }

        public void reconnectInput()
        {
            List<String> auto_connect = new List<string>() { "Moog Claravox", "Widi Master Bluetooth", "Network Session 1", 
                 // TODO: remove Minitaur
                "Moog Minitaur"};
            List<String> in_devices = getInputPorts();
            bool changed_in = false;

            foreach (String name in auto_connect)
            {
                for (int i = 0; i < in_devices.Count(); ++i)
                {
                    if (in_devices[i].Contains(name))
                    {
                        //Console.Out.WriteLine("MIDI connected: " + name);
                        //NSLog(@"MIDI connected: %@", [NSString stringWithUTF8String: devices[i].toUTF8()]);
                        openInputDevice(i);
                        changed_in = true;
                        break;
                    }
                }
            }
        }

        public void reconnectOutput()
        {
            List<String> auto_connect = new List<string>() { "Moog Claravox", "Widi Master Bluetooth", "Network Session 1", 
                 // TODO: remove Minitaur
                "Moog Minitaur"};
            List<String> out_devices = getOutputPorts();
            bool changed_out = false;

            foreach (String name in auto_connect)
            {
                for (int i = 0; i < out_devices.Count(); ++i)
                {
                    if (out_devices[i].Contains(name))
                    {
                        //Console.Out.WriteLine("MIDI connected: " + name);
                        //NSLog(@"MIDI connected: %@", [NSString stringWithUTF8String: devices[i].toUTF8()]);
                        openOutputDevice(i);
                        changed_out = true;

                        break;
                    }
                }

                if (changed_out)
                {
                    break;
                }
            }
        }
    }
}
