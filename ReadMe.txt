Claravox Editor for Universal WIndows Plateform (UWP) 

Language:
Xaml with C# for code-behind

Requirements :
- Windows 10
- Microsoft Visual Studio 2019
- Newtonsoft.Json NuGet package (tutorial : https://moredvikas.wordpress.com/2019/03/06/add-the-newtonsoft-json-nuget-package-in-visual-studio-to-dot-net-project)
- Microsoft.Toolkit.Uwp.UI NuGet package (if needed, use https://api.nuget.org/v3/index.json as source in Nuget Package to install)
- UWP sdk (from Visual Studio 2019 installer)

Steps: 
1. Open the Claravox solution with Microsoft Visual Studio 2019
2. Set build configuration as Debug x64
3. Clean solution
4. UWP applications have restricted access to user's hard drive. 
As such, presets (or any resources files needed at runtime) needs to be located in specific directory like localState/roaming etc... 
Final build on user's computer will use standard path. 
But for local debug build please manually install presets under "ApplicationData.Current.LocalFolder" : this translate in my case to "C:\Users\Moog\AppData\Local\Packages\84bdce18-45fd-44ab-83e4-9ba5220c09e3_mm3hgy48qawaj\LocalState\Presets". The path used is exposed as String in PresetManager::readPresets() for convenience.
5. Build and run