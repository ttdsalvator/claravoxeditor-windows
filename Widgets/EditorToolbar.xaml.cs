﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

using Claravox.Data;

// Pour en savoir plus sur le modèle d'élément Contrôle utilisateur, consultez la page https://go.microsoft.com/fwlink/?LinkId=234236

namespace Claravox.Widgets
{
    public sealed partial class EditorToolbar : UserControl
    {
        private MainPage mainPagePtr_;

        public EditorToolbar()
        {
            mainPagePtr_ = MainPage.Current;

            this.InitializeComponent();            
            updatePanelButtons();
        }

        //TODO: Reimplement
        //private void UndoClicked(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        //{

        //}

        //private void RedoClicked(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        //{

        //}

        private void PrevPresetClicked(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            mainPagePtr_.presetManager_.prevPreset();
        }

        private void NextPresetClicked(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            mainPagePtr_.presetManager_.nextPreset();
        }

        private void SaveClicked(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
        }

        private void SaveAsClicked(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
        }

        private void PresetClicked(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            mainPagePtr_.showPanel(MainPage.PanelsType.panelLibrary);
            updatePanelButtons();
        }

        private void ParametersBtnClicked(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            mainPagePtr_.showPanel(MainPage.PanelsType.panelParameter);
            updatePanelButtons();
        }

        private void LibraryBtnClicked(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            mainPagePtr_.showPanel(MainPage.PanelsType.panelLibrary);
            updatePanelButtons();
        }

        private void SettingsBtnClicked(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            mainPagePtr_.showPanel(MainPage.PanelsType.panelSettings);
            updatePanelButtons();
        }

        private void updatePanelButtons()
        {
            parametersButton.IsChecked  = mainPagePtr_.activePanel_ == MainPage.PanelsType.panelParameter;
            libraryButton.IsChecked     = mainPagePtr_.activePanel_ == MainPage.PanelsType.panelLibrary;
            settingsButton.IsChecked    = mainPagePtr_.activePanel_ == MainPage.PanelsType.panelSettings;
        }
    }
}
