﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using Microsoft.Toolkit.Uwp.Helpers;
using Windows.UI.Xaml;
using System.Threading.Tasks;
using Windows.UI.Core;
using Windows.UI.Xaml.Controls;
using Windows.Devices.Midi;
using Windows.Storage.Streams;
using System.Collections.ObjectModel;

using System.ComponentModel;
using Claravox.MIDI;
using System.Threading;

//using Windows.ApplicationModel.Core;
//using Windows.UI.Core;

//public static async Task CallOnUiThreadAsync(CoreDispatcher dispatcher, DispatchedHandler handler) =>
//  await dispatcher.RunAsync(CoreDispatcherPriority.Normal, handler);

namespace Claravox.Models
{
    public class ParameterCC : INotifyPropertyChanged
    {
        int cc_;
        public double value_;
        bool quantized_;
        bool isMSB_ = false;
        const String valueName_ = "value";

        public ParameterCC(int cc, bool quantized)
        {
            this.cc_ = cc;
            this.quantized_ = quantized;
            this.value_ = 0.0;
        }

        public double value
        {
            get { return this.getParameter(); }
            set
            {
                value_ = value;
                storeParameterValue(value_);
                valueChanged();
                //if (midiState_ != null)
                //{
                //    midiState_.sendParameter(cc_, value_);
                //}
            }
        }

        public void valueChanged()
        {
            OnPropertyChanged(valueName_);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public void setMidiState(MidiState state)
        {
            this.midiState_ = state;
            //Register itself in MidiState's parameterCCs array. Maybe there's a best way to add to the array ?
            state.parameterCCs[cc_] = this;
        }

        public double ccToDouble(int ccVal)
        {
            var value = (double)(ccVal);
            if (this.quantized_)
            {
                value = value / 128.0;
            }
            else
            {
                value = value / 127.0; ;
            }
            value = Math.Min(Math.Max(value, 0.0), 1.0);
            return value;
        }

        public double retrieveParameterValue()
        {
            if (midiState_ != null)
            {
                // TODO: 
                return ccToDouble(midiState_.getParameterValue(cc_));
                //return (midiState_.getParameterValue(cc_));
            }
            else
            {
                return ccToDouble(0);
            }
        }

        public double getParameter()
        {
            return retrieveParameterValue();
        }

        public void storeParameterValue(double newValue)
        {
            // TODO:
            //if let cb = callback {
            //    cb(newValue)
            //    }

            var cc_value = 0;
            var msbValue = 0;
            var lsbValue = 0;

            bool is14Bit = false;
            // TODO: midiState?.outputResolution.is14Bit();

            if (is14Bit)
            {
                // 14 bit
                if (is14Bit && isMSB_)
                {
                    if (quantized_)
                    {
                        cc_value = (int)(Math.Ceiling(newValue * 16384.0));
                    }
                    else
                    {
                        cc_value = (int)(newValue * 16383.0);
                    }

                    cc_value = Math.Min(Math.Max(cc_value, 0), 16383);
                    msbValue = cc_value >> 7 & 127;
                    lsbValue = cc_value & 127;
                    midiState_.sendParameter(cc_, value: msbValue);
                    midiState_.sendParameter(cc_ + 32, value: lsbValue);
                }
                // 7 bit
            }
            else
            {
                if (quantized_)
                {
                    cc_value = (int)(Math.Ceiling(newValue * 128.0));
                }
                else
                {
                    cc_value = (int)(newValue * 127.0);
                }
                cc_value = Math.Min(Math.Max(cc_value, 0), 127);
                midiState_.sendParameter(cc_, cc_value);
            }
        }

        public void sendParameter(double newValue)
        {
            this.storeParameterValue(newValue);

            //DispatchQueue.main.async {
            //    self.objectWillChange.send()
            //    }
        }

        MidiState midiState_;
        PresetManager presetManager_;
    }

    public class MIDIResolution : BindableBase
    {
        bool is14Bit_ = true;
        public bool is14Bit
        {
            get { return is14Bit_; }
            set
            {
                is14Bit_ = value;
                OnPropertyChanged();
            }
        }
    }

    // ---------------------- MIDI STATE ----------------------
    public class MidiState : Page   // inherits Page to get the Dispatcher
    {
        public ObservableCollection<ParameterCC> Params
        {
            get { return this.parameterCCs; }
        }

        public MidiState()
        {
            for (int i = 0; i < 128; i++)
            {
                parameterCCs.Add(new ParameterCC(i, false));
            }

            init();
        }

        public void init()
        {
            //this.appChannel.setMidiState(this); ;
            //this.midiInChannel.setMidiState(this);
            //this.midiOutChannel.setMidiState(this);

            // !!!
            // ensure that every ParameterCC instance has its midiState property set here;
            // !!!
            this.filter.setMidiState(this);
            this.brightness.setMidiState(this);
            this.wave.setMidiState(this);
            this.delay.setMidiState(this);
            this.feedback.setMidiState(this);
            this.mix.setMidiState(this);
            this.volumeCurve.setMidiState(this);
            this.pitchCurve.setMidiState(this);
            this.quantize.setMidiState(this);
            this.root.setMidiState(this);
            this.scale.setMidiState(this);
            this.octave.setMidiState(this);
            this.osc1type.setMidiState(this);
            this.osc1level.setMidiState(this);
            this.osc1wavetable.setMidiState(this);
            this.osc1scanFreq.setMidiState(this);
            this.osc1scanPos.setMidiState(this);
            this.osc1scanAmt.setMidiState(this);
            this.osc2type.setMidiState(this);
            this.osc2level.setMidiState(this);
            this.osc2beatFrequency.setMidiState(this);
            this.noiseLevel.setMidiState(this);
            this.osc2wavetable.setMidiState(this);
            this.osc2scanFreq.setMidiState(this);
            this.osc2scanPos.setMidiState(this);
            this.osc2scanAmt.setMidiState(this);
            this.osc2FilterFreq.setMidiState(this);
            this.osc2FilterRes.setMidiState(this);
            this.osc2FilterMode.setMidiState(this);
            this.osc2FilterEnable.setMidiState(this);
            this.pitchModWsFreq.setMidiState(this);
            this.pitchModWsAmt.setMidiState(this);
            this.osc1filterPitchAmt.setMidiState(this);
            this.osc2filterPitchAmt.setMidiState(this);
            this.osc1filterVolumeAmt.setMidiState(this);
            this.osc2filterVolumeAmt.setMidiState(this);
            
            this.cvInOffset.setMidiState(this);
            this.cvInputRange.setMidiState(this);
            this.cvInputScale.setMidiState(this);
            this.volumeCvOutScale.setMidiState(this);
            this.volumeCvOutRange.setMidiState(this);
            
            this.pitchCvOutRange.setMidiState(this);
            this.pitchCvQuant.setMidiState(this);
            
            // !!!;
            // ensure that every OptionSelector and OptionAction instance has its midiState property set here;
            // !!!;
            //this.serviceReporting.setMidiState(this);
            //this.serviceCalibration.setMidiState(this);
            //this.serviceFactoryTest.setMidiState(this);
            //this.testResult.setMidiState(this);
            //this.testPreset.setMidiState(this);
            //_potCalLow.setMidiState(this);
            //_potCalHigh.setMidiState(this);
            //_testEeprom.setMidiState(this);
            //_testMidiLoopBack.setMidiState(this);
            //_testAudioNextPreset.setMidiState(this);
            //_testAudioPrevPreset.setMidiState(this);

            // !!!
            // ensure that every ServiceNRPN instance has its midiState property set here;
            // !!!
            //this.pitchOscFreq.setMidiState(this);
            //this.pitchHetFreq.setMidiState(this);
            //this.pitchResponseCurve.setMidiState(this);
            //this.pitchNearFreq.setMidiState(this);
            //this.pitchFarFreq.setMidiState(this);
            //this.pitchQuantStartNote.setMidiState(this);
            //this.pitchQuantEndNote.setMidiState(this);

            //this.volInputADC.setMidiState(this);
            //this.volScaled.setMidiState(this);
            //this.volRectGain.setMidiState(this);
            //this.volRectOffset.setMidiState(this);
            //this.volNear.setMidiState(this);
            //this.volFar.setMidiState(this);
            //this.volCvWindow.setMidiState(this);
            //this.volCvOffset.setMidiState(this);
            //this.volRectRaw.setMidiState(this);
            //this.volRectVca.setMidiState(this);

            //this.bbdGain.setMidiState(this);
            //this.bbdOffset.setMidiState(this);

            //this.pot1Lo.setMidiState(this);
            //this.pot1Hi.setMidiState(this);
            //this.pot1Val.setMidiState(this);
            //this.pot2Lo.setMidiState(this);
            //this.pot2Hi.setMidiState(this);
            //this.pot2Val.setMidiState(this);
            //this.pot3Lo.setMidiState(this);
            //this.pot3Hi.setMidiState(this);
            //this.pot3Val.setMidiState(this);
            //this.pot4Lo.setMidiState(this);
            //this.pot4Hi.setMidiState(this);
            //this.pot4Val.setMidiState(this);
            //this.pot5Lo.setMidiState(this);
            //this.pot5Hi.setMidiState(this);
            //this.pot5Val.setMidiState(this);
            //this.pot6Lo.setMidiState(this);
            //this.pot6Hi.setMidiState(this);
            //this.pot6Val.setMidiState(this);
            //this.pot7Lo.setMidiState(this);
            //this.pot7Hi.setMidiState(this);
            //this.pot7Val.setMidiState(this);
            //this.pot8Lo.setMidiState(this);
            //this.pot8Hi.setMidiState(this);
            //this.pot8Val.setMidiState(this);
            //this.pot9Lo.setMidiState(this);
            //this.pot9Hi.setMidiState(this);
            //this.pot9Val.setMidiState(this);
            //this.pot10Lo.setMidiState(this);
            //this.pot10Hi.setMidiState(this);
            //this.pot10Val.setMidiState(this);
            //this.pot11Lo.setMidiState(this);
            //this.pot11Hi.setMidiState(this);
            //this.pot11Val.setMidiState(this);
            //this.swOctave.setMidiState(this);
            //this.swTimbre.setMidiState(this);
            //this.swTimbreStore.setMidiState(this);
            //this.swRootSet.setMidiState(this);
            //this.swMuteRear.setMidiState(this);
            //this.swMuteFront.setMidiState(this);
            //this.swTraditional.setMidiState(this);
            //this.cvInput.setMidiState(this);
        }

        public void configure(MidiManager midiManager, PresetManager presetManager)
        {
            midiManagerPtr_ = midiManager;
            presetManagerPtr_ = presetManager;

            //configure(midiManager: MidiManager, presetManager: PresetManager)
            //{
            //    self.midiManager = midiManager
            //self.presetManager = presetManager
            //midiManager.setDelegate(self)
            //NotificationCenter.default.addObserver(self, selector: #selector(flushPreset), name: Notification.Name("FlushPreset"), object: nil)

            foreach (ParameterCC param in parameterCCs)
            {
                // TODO: configure callback to presetManager
                //if presetManager.preset != nil {
                //    self.filter.callback = { value in setPresetParam(&(presetManager.preset!.parameters.filterVal), value: value) }
                //    self.brightness.callback = { value in setPresetParam(&(presetManager.preset!.parameters.brightVal), value: value) }
                // param.Add(new ParameterCC(i, false));
            }

            editorAnnounce();
        }

        public void sendParameter(int number, int value)
        {
            if (validCC(number) && value != lastReceivedCCs_[number])
            {
                lastReceivedCCs_[number] = value;

                if (midiManagerPtr_ != null)
                {
                    if (lastSentCCs_[number] != value)
                    {
                        lastSentCCs_[number] = value;
                        midiManagerPtr_.sendCC(appChannel_, number, value);
                    }
                }
            }
        }

        public void sendNRPN(int number, int value)
        {
            if (midiManagerPtr_ != null)
            {
                // midiManagerPtr_.sendNRPN(appChannel_, number, value);
            }
        }

        public async void receivedCC(int number, int value, int channel)
        {
            if (validCC(number) && validateInputChannel(channel))
            {
                if (this.lastReceivedCCs_[number] != value)
                {
                    lastReceivedCCs_[number] = value;

                    switch (number)
                    {
                        case CC_NRPN_MSB:
                            nrpnMsb_ = value;
                            break;
                        case CC_NRPN_LSB:
                            nrpnLsb_ = value;
                            break;
                        case CC_DATA_ENTRY_MSB:
                            dataMsb_ = value;
                            break;
                        case CC_DATA_ENTRY_LSB:
                            dataLsb_ = value;
                            handleNrpnData();
                            break;
                        default:
                            break;
                    };

                    if (number < this.parameterCCs.Count && this.parameterCCs[number] != null)
                    {
                        // TODO: to use Dispatcher and update UI, we had to make this class a PAGE, which is a hack
                        Dispatcher.RunAsync(CoreDispatcherPriority.High, () =>
                        {
                            ParameterCC param = parameterCCs[number];
                            //param.Value = (Int32)value;
                            //param.PropertyChanged?.Invoke(param, new PropertyChangedEventArgs("TimeStamp"));
                            //PropertyChangedEventHandler handler = param.PropertyChange.PropertyChanged;
                            //if (handler != null) handler(param, new PropertyChangedEventArgs("TimeStamp"));
                            param.valueChanged();
                        });

                        //DispatchQueue.main.async
                        //    {
                        //    paramcc.objectWillChange.send();
                        //}

                        //if (cb = paramcc.callback)
                        //{
                        //    cb(paramcc.ccToDouble(value));
                        //}
                    }
                }
            }
        }

        public void handleNrpnData()
        {
            if (nrpnMsb_ == 127 && nrpnLsb_ == 127)
            {
                return;
            }

            int nrpn = (nrpnMsb_ << 7 | nrpnLsb_);
            int data = (dataMsb_ << 7 | dataLsb_);

            //if (nrpn < serviceNRPNs.Count())
            //{
            //    if (int service_nrpn = serviceNRPNs[nrpn])
            //    {
            //        service_nrpn.updateValue(nrpn: nrpn, data: data);
            //        DispatchQueue.main.async
            //        {
            //            service_nrpn.objectWillChange.send();
            //        }
            //    }
            //}
        }

        public int getParameterValue(int number)
        {
            return lastReceivedCCs_[number];
        }

        private bool validCC(int number)
        {
            return number >= 0 && number <= 127;
        }

        // chanel 0 being omni
        private bool validChannel(int channel)
        {
            return channel >= 0 && channel <= 16;
        }

        private bool validateInputChannel(int channel)
        {
            return appChannel_ == 0 || channel == appChannel_;
        }

        public void flushPreset()
        {
            if (presetManagerPtr_ != null)
            {
                Claravox.Data.Preset p = presetManagerPtr_.focusedPreset;

                if (p != null)
                {
                    //forceSend = true;
                    //let params = preset.parameters
                    filter.value = p.Parameters.GetValueOrDefault("filterVal");
                    brightness.value = p.Parameters.GetValueOrDefault("brightVal");
                    wave.value = p.Parameters.GetValueOrDefault("waveVal");
                    delay.value = p.Parameters.GetValueOrDefault("timeVal");
                    feedback.value = p.Parameters.GetValueOrDefault("feedbackVal");
                    mix.value = p.Parameters.GetValueOrDefault("delayAmount");
                    volumeCurve.value = p.Parameters.GetValueOrDefault("volCurveVal");
                    pitchCurve.value = p.Parameters.GetValueOrDefault("pitchCurveVal");
                    quantize.value = p.Parameters.GetValueOrDefault("quantizeAmount");
                    root.value = p.Parameters.GetValueOrDefault("rootVal");
                    scale.value = p.Parameters.GetValueOrDefault("scaleVal");
                    octave.value = p.Parameters.GetValueOrDefault("octaveState");
                    osc1type.value = p.Parameters.GetValueOrDefault("osc1Type");
                    osc1level.value = p.Parameters.GetValueOrDefault("osc1Level");
                    osc1wavetable.value = p.Parameters.GetValueOrDefault("osc1Wavetable");
                    osc1scanFreq.value = p.Parameters.GetValueOrDefault("osc1ScanFreq");
                    osc1scanPos.value = p.Parameters.GetValueOrDefault("osc1ScanPos");
                    osc1scanAmt.value = p.Parameters.GetValueOrDefault("osc1ScanAmount");
                    osc2type.value = p.Parameters.GetValueOrDefault("osc2Type");
                    osc2level.value = p.Parameters.GetValueOrDefault("osc2Level");
                    osc2beatFrequency.value = p.Parameters.GetValueOrDefault("osc2Freq");
                    noiseLevel.value = p.Parameters.GetValueOrDefault("noiseLevel");
                    osc2wavetable.value = p.Parameters.GetValueOrDefault("osc2Wavetable");
                    osc2scanFreq.value = p.Parameters.GetValueOrDefault("osc2ScanFreq");
                    osc2scanPos.value = p.Parameters.GetValueOrDefault("osc2ScanPos");
                    osc2scanAmt.value = p.Parameters.GetValueOrDefault("osc2ScanAmount");
                    osc2FilterFreq.value = p.Parameters.GetValueOrDefault("osc2FilterFreq");
                    osc2FilterRes.value = p.Parameters.GetValueOrDefault("osc2FilterRes");
                    osc2FilterMode.value = p.Parameters.GetValueOrDefault("osc2FilterMode");
                    osc2FilterEnable.value = p.Parameters.GetValueOrDefault("osc2FilterEnable");
                    pitchModWsFreq.value = p.Parameters.GetValueOrDefault("pitchModWsFreq");
                    pitchModWsAmt.value = p.Parameters.GetValueOrDefault("pitchModWsAmount");
                    osc1filterPitchAmt.value = p.Parameters.GetValueOrDefault("osc1FilterPitchAmount");
                    osc2filterPitchAmt.value = p.Parameters.GetValueOrDefault("osc2FilterPitchAmount");
                    osc1filterVolumeAmt.value = p.Parameters.GetValueOrDefault("osc1FilterVolumeAmount");
                    osc2filterVolumeAmt.value = p.Parameters.GetValueOrDefault("osc1FilterVolumeAmount");
                    
                    // TODO: 
                    //midiPitchOut.value = preset.Parameters.GetValueOrDefault("midiPitchOut");
                    //midiNoteNumber.value = preset.Parameters.GetValueOrDefault("midiNoteNumber");
                    //midiNoteNumberMode.value = preset.Parameters.GetValueOrDefault("midiNoteNumberMode");
                    //forceSend = false;
                }
            }
        }

        // ------------------ variables ------------------
        public ObservableCollection<ParameterCC> parameterCCs = new ObservableCollection<ParameterCC>();
        
        public ParameterCC filter = new ParameterCC(19, false);
        public ParameterCC brightness = new ParameterCC(17, false);
        public ParameterCC wave = new ParameterCC(18, false);
        public ParameterCC delay = new ParameterCC(20, false);
        public ParameterCC feedback = new ParameterCC(22, false);
        public ParameterCC mix = new ParameterCC(21, false);
        public ParameterCC volumeCurve = new ParameterCC(12, false);
        public ParameterCC pitchCurve = new ParameterCC(13, false);
        public ParameterCC quantize = new ParameterCC(16, false);
        public ParameterCC root = new ParameterCC(85, false);
        public ParameterCC scale = new ParameterCC(86, true);
        public ParameterCC octave = new ParameterCC(75, true);
        public ParameterCC osc1type = new ParameterCC(81, true);
        public ParameterCC osc1level = new ParameterCC(25, false);
        public ParameterCC osc1wavetable = new ParameterCC(83, true);
        public ParameterCC osc1scanFreq = new ParameterCC(23, false);
        public ParameterCC osc1scanPos = new ParameterCC(87, false);
        public ParameterCC osc1scanAmt = new ParameterCC(89, false);
        public ParameterCC osc2type = new ParameterCC(80, true);
        public ParameterCC osc2level = new ParameterCC(26, false);
        public ParameterCC osc2beatFrequency = new ParameterCC(28, false);
        public ParameterCC noiseLevel = new ParameterCC(9, false);
        public ParameterCC osc2wavetable = new ParameterCC(82, true);
        public ParameterCC osc2scanFreq = new ParameterCC(24, false);
        public ParameterCC osc2scanPos = new ParameterCC(88, false);
        public ParameterCC osc2scanAmt = new ParameterCC(90, false);
        public ParameterCC osc2FilterFreq = new ParameterCC(8, false);
        public ParameterCC osc2FilterRes = new ParameterCC(10, false);
        public ParameterCC osc2FilterMode = new ParameterCC(91, true);
        public ParameterCC osc2FilterEnable = new ParameterCC(103, true);
        public ParameterCC pitchModWsFreq = new ParameterCC(72, false);
        public ParameterCC pitchModWsAmt = new ParameterCC(73, false);
        public ParameterCC osc1filterPitchAmt = new ParameterCC(2, false);
        public ParameterCC osc2filterPitchAmt = new ParameterCC(3, false);
        public ParameterCC osc1filterVolumeAmt = new ParameterCC(4, false);
        public ParameterCC osc2filterVolumeAmt = new ParameterCC(5, false);
        public ParameterCC midiPitchOut = new ParameterCC(108, true);
        public ParameterCC midiNoteNumber = new ParameterCC(109, false);
        public ParameterCC midiNoteNumberMode = new ParameterCC(110, true); 

        public ParameterCC cvInOffset = new ParameterCC(29, false);      
        public ParameterCC cvInputRange = new ParameterCC(92, true);
        public ParameterCC cvInputScale = new ParameterCC(14, false);
        public ParameterCC volumeCvOutScale = new ParameterCC(30, false);
        public ParameterCC volumeCvOutRange = new ParameterCC(104, true);
        
        public ParameterCC pitchCvOutRange = new ParameterCC(93, true);
        public ParameterCC pitchCvQuant = new ParameterCC(95, true);
        public ParameterCC rearMute = new ParameterCC(107, true);

        // TODO:
        //var serviceReporting = OptionSelector(nrpnOff: 1006, nrpnOn: 1007)
        //var serviceCalibration = OptionSelector(nrpnOff: 1003, nrpnOn: 1004)
        //var serviceFactoryTest = OptionSelector(nrpnOff: 1009, nrpnOn: 1010)


        //@OptionAction(nrpn:1001) var potCalLow
        //@OptionAction(nrpn:1002) var potCalHigh


        //@OptionAction(nrpn:2403) var testEeprom
        //@OptionAction(nrpn:2404) var testMidiLoopBack
        //@OptionAction(nrpn:2405) var testAudioNextPreset
        //@OptionAction(nrpn:2406) var testAudioPrevPreset


        //var testResult = ServiceNRPN(nrpnMsw: 2401, nrpnLsw: 2402, normalized: false)
        //var testPreset = ServiceNRPN(nrpnMsw: 2407, nrpnLsw: 2408, normalized: false)

        //var pitchOscFreq = ServiceNRPN(nrpnMsw: 2001, nrpnLsw: 2002, normalized: false)
        //var pitchHetFreq = ServiceNRPN(nrpnMsw: 2003, nrpnLsw: 2004, normalized: false)
        //var pitchResponseCurve = ServiceNRPN(nrpnMsw: 2005, nrpnLsw: 2006, normalized: true)
        //var pitchNearFreq = ServiceNRPN(nrpnMsw: 2007, nrpnLsw: 2008, normalized: false)
        //var pitchFarFreq = ServiceNRPN(nrpnMsw: 2009, nrpnLsw: 2010, normalized: false)
        //var pitchQuantStartNote = ServiceNRPN(nrpnMsw: 2011, nrpnLsw: 2012, normalized: false)
        //var pitchQuantEndNote = ServiceNRPN(nrpnMsw: 2013, nrpnLsw: 2014, normalized: false)


        //var volInputADC = ServiceNRPN(nrpnMsw: 2101, nrpnLsw: 2102, normalized: false)
        //var volScaled = ServiceNRPN(nrpnMsw: 2103, nrpnLsw: 2104, normalized: true)
        //var volRectGain = ServiceNRPN(nrpnMsw: 2105, nrpnLsw: 2106, normalized: true)
        //var volRectOffset = ServiceNRPN(nrpnMsw: 2107, nrpnLsw: 2108, normalized: true)
        //var volNear = ServiceNRPN(nrpnMsw: 2109, nrpnLsw: 2110, normalized: true)
        //var volFar = ServiceNRPN(nrpnMsw: 2111, nrpnLsw: 2112, normalized: true)
        //var volCvWindow = ServiceNRPN(nrpnMsw: 2113, nrpnLsw: 2114, normalized: true)
        //var volCvOffset = ServiceNRPN(nrpnMsw: 2115, nrpnLsw: 2116, normalized: true)
        //var volRectRaw = ServiceNRPN(nrpnMsw: 2117, nrpnLsw: 2118, normalized: true)
        //var volRectVca = ServiceNRPN(nrpnMsw: 2119, nrpnLsw: 2120, normalized: true)


        //var bbdGain = ServiceNRPN(nrpnMsw: 2201, nrpnLsw: 2202, normalized: true)
        //var bbdOffset = ServiceNRPN(nrpnMsw: 2203, nrpnLsw: 2204, normalized: true)


        //var pot1Lo = ServiceNRPN(nrpnMsw: 2301, nrpnLsw: 2302, normalized: false)
        //var pot1Hi = ServiceNRPN(nrpnMsw: 2303, nrpnLsw: 2304, normalized: false)
        //var pot1Val = ServiceNRPN(nrpnMsw: 2305, nrpnLsw: 2306, normalized: true)
        //var pot2Lo = ServiceNRPN(nrpnMsw: 2307, nrpnLsw: 2308, normalized: false)
        //var pot2Hi = ServiceNRPN(nrpnMsw: 2309, nrpnLsw: 2310, normalized: false)
        //var pot2Val = ServiceNRPN(nrpnMsw: 2311, nrpnLsw: 2312, normalized: true)
        //var pot3Lo = ServiceNRPN(nrpnMsw: 2313, nrpnLsw: 2314, normalized: false)
        //var pot3Hi = ServiceNRPN(nrpnMsw: 2315, nrpnLsw: 2316, normalized: false)
        //var pot3Val = ServiceNRPN(nrpnMsw: 2317, nrpnLsw: 2318, normalized: true)
        //var pot4Lo = ServiceNRPN(nrpnMsw: 2319, nrpnLsw: 2320, normalized: false)
        //var pot4Hi = ServiceNRPN(nrpnMsw: 2321, nrpnLsw: 2322, normalized: false)
        //var pot4Val = ServiceNRPN(nrpnMsw: 2323, nrpnLsw: 2324, normalized: true)
        //var pot5Lo = ServiceNRPN(nrpnMsw: 2325, nrpnLsw: 2326, normalized: false)
        //var pot5Hi = ServiceNRPN(nrpnMsw: 2327, nrpnLsw: 2328, normalized: false)
        //var pot5Val = ServiceNRPN(nrpnMsw: 2329, nrpnLsw: 2330, normalized: true)
        //var pot6Lo = ServiceNRPN(nrpnMsw: 2331, nrpnLsw: 2332, normalized: false)
        //var pot6Hi = ServiceNRPN(nrpnMsw: 2333, nrpnLsw: 2334, normalized: false)
        //var pot6Val = ServiceNRPN(nrpnMsw: 2335, nrpnLsw: 2336, normalized: true)
        //var pot7Lo = ServiceNRPN(nrpnMsw: 2337, nrpnLsw: 2338, normalized: false)
        //var pot7Hi = ServiceNRPN(nrpnMsw: 2339, nrpnLsw: 2340, normalized: false)
        //var pot7Val = ServiceNRPN(nrpnMsw: 2341, nrpnLsw: 2342, normalized: true)
        //var pot8Lo = ServiceNRPN(nrpnMsw: 2343, nrpnLsw: 2344, normalized: false)
        //var pot8Hi = ServiceNRPN(nrpnMsw: 2345, nrpnLsw: 2346, normalized: false)
        //var pot8Val = ServiceNRPN(nrpnMsw: 2347, nrpnLsw: 2348, normalized: true)
        //var pot9Lo = ServiceNRPN(nrpnMsw: 2349, nrpnLsw: 2350, normalized: false)
        //var pot9Hi = ServiceNRPN(nrpnMsw: 2351, nrpnLsw: 2352, normalized: false)
        //var pot9Val = ServiceNRPN(nrpnMsw: 2353, nrpnLsw: 2354, normalized: true)
        //var pot10Lo = ServiceNRPN(nrpnMsw: 2355, nrpnLsw: 2356, normalized: false)
        //var pot10Hi = ServiceNRPN(nrpnMsw: 2357, nrpnLsw: 2358, normalized: false)
        //var pot10Val = ServiceNRPN(nrpnMsw: 2359, nrpnLsw: 2360, normalized: true)
        //var pot11Lo = ServiceNRPN(nrpnMsw: 2361, nrpnLsw: 2362, normalized: false)
        //var pot11Hi = ServiceNRPN(nrpnMsw: 2363, nrpnLsw: 2364, normalized: false)
        //var pot11Val = ServiceNRPN(nrpnMsw: 2365, nrpnLsw: 2366, normalized: true)


        //var swOctave = ServiceNRPN(nrpnMsw: 2367, nrpnLsw: 2368, normalized: false)
        //var swTimbre = ServiceNRPN(nrpnMsw: 2369, nrpnLsw: 2370, normalized: false)
        //var swTimbreStore = ServiceNRPN(nrpnMsw: 2371, nrpnLsw: 2372, normalized: false)
        //var swRootSet = ServiceNRPN(nrpnMsw: 2373, nrpnLsw: 2374, normalized: false)
        //var swMuteRear = ServiceNRPN(nrpnMsw: 2375, nrpnLsw: 2376, normalized: false)
        //var swMuteFront = ServiceNRPN(nrpnMsw: 2377, nrpnLsw: 2378, normalized: false)
        //var swTraditional = ServiceNRPN(nrpnMsw: 2379, nrpnLsw: 2380, normalized: false)
        //var cvInput = ServiceNRPN(nrpnMsw: 2381, nrpnLsw: 2382, normalized: false)

        MIDIResolution outputResolution = new MIDIResolution();

        public enum ParameterIds
        {
            // TODO: consider using an enum and setup maps with id/title etc... ?
            param1 = 0,
            param22 = 22,

        };
        private bool forceSend = false;

        int appChannel_ = 0; //var appChannel = MidiChannel(channel: 1)
        int[] lastCCValue_ = new int[128];
        int[] lastSentCCs_ = new int[128]; //TODO: repeating: -1,
        int[] lastReceivedCCs_ = new int[128];
        int dataMsb_ = 0;
        int dataLsb_ = 0;
        int nrpnMsb_ = 127;
        int nrpnLsb_ = 127;
        const int CC_NRPN_MSB = 99;
        const int CC_NRPN_LSB = 98;
        const int CC_DATA_ENTRY_MSB = 6;
        const int CC_DATA_ENTRY_LSB = 38;

        MidiManager midiManagerPtr_;
        PresetManager presetManagerPtr_;


        void editorAnnounce()
        {
            //if let midiExt = self.midiExtProtocol {
            //    midiExt.sendEditorAnnounce()
            forceSend = true;
            loadDefaultGlobals();
            forceSend = false;
            //}
        }
   
        void loadDefaultGlobals()
        {
            UserDefaults settings = UserDefaults.Current;
            if (settings != null)
                {
                    if (settings.isKeyPresentInUserDefaults("midiInChannel"))
                    {
                        this.midiInChannel = settings.Read<int>("midiInChannel");
                    }

                    if (settings.isKeyPresentInUserDefaults(key: "midiOutChannel"))
                    {
                        this.midiOutChannel = settings.Read<int>("midiOutChannel");
                    }

                    if (settings.isKeyPresentInUserDefaults(key: "appChannel"))
                    {
                        this.appChannel = settings.Read<int>("appChannel");
                    }

                    if (settings.isKeyPresentInUserDefaults(key: "outputResolution"))
                    {
                        this.outputResolution.is14Bit = settings.Read<bool>("outputResolution");
                    }

                    if (!settings.isKeyPresentInUserDefaults(key: "hideNumberValues"))
                    {
                        settings.Store("hideNumberValues", false);
                    }

                    // MARK: CV Globals

                    if (settings.isKeyPresentInUserDefaults(key: "cvInOffset"))
                    {
                        this.cvInOffset.value = settings.Read<double>("cvInOffset");
                    }

                    if (settings.isKeyPresentInUserDefaults(key: "cvInputRange"))
                    {
                        this.cvInputRange.value = settings.Read<double>("cvInputRange");
                    }

                    if (settings.isKeyPresentInUserDefaults(key: "cvInputScale"))
                    {
                        this.cvInputScale.value = settings.Read<double>("cvInputScale");
                    }

                    if (settings.isKeyPresentInUserDefaults(key: "volumeCvOutScale"))
                    {
                        this.volumeCvOutScale.value = settings.Read<double>("volumeCvOutScale");
                    }

                    if (settings.isKeyPresentInUserDefaults(key: "volumeCvOutRange"))
                    {
                        this.volumeCvOutRange.value = settings.Read<double>("volumeCvOutRange");
                    }

                    if (settings.isKeyPresentInUserDefaults(key: "pitchCvOutRange"))
                    {
                        this.pitchCvOutRange.value = settings.Read<double>("pitchCvOutRange");
                    }

                    if (settings.isKeyPresentInUserDefaults(key: "pitchCvQuant"))
                    {
                        this.pitchCvQuant.value = settings.Read<double>("pitchCvQuant");
                    }

                }
            
        }
        //private int _lastCCValue;
        //public int CCValue
        //{
        //    get { return _lastCCValue; }
        //    set
        //    {
        //        _lastCCValue = value;
        //        RaisePropertyChanged("CCValue");
        //    }
        //}

        //public event PropertyChangedEventHandler PropertyChanged;
        //protected void RaisePropertyChanged(string name)
        //{
        //    if (PropertyChanged != null)
        //    {
        //        PropertyChanged(this, new PropertyChangedEventArgs(name));
        //    }
        //}

        // local variables
        int appChannel = 1;
        int midiInChannel = 0; //MidiChannel(channel: 0)
        int midiOutChannel = 1; //MidiChannel(channel: 1)
    }
}
