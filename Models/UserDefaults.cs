﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace Claravox.Models
{
    public class UserDefaults : INotifyPropertyChanged
    {
        public static UserDefaults Current;
  
        public bool isValueVisible
        {
            get
            {
                return !hideNumberValues;
            }
        }

        public bool hideNumberValues
        {
            get
            {
                return Read<bool>(nameof(hideNumberValues));
            }
            set
            {
                Store(nameof(hideNumberValues), value);
                NotifyPropertyChanged();
            }
        }

        public ApplicationDataContainer LocalSettings { get; set; }

        // Constructor
        public UserDefaults()
        {
            LocalSettings = ApplicationData.Current.LocalSettings;
            Current = this;
        }

        public T Read<T>(string key)
        {
            if (LocalSettings.Values.ContainsKey(key))
            {
                return (T)LocalSettings.Values[key];
            }

            return default(T);
        }

        public void Store(string key, object value)
        {
            LocalSettings.Values[key] = value;
        }

        public void Remove(string key)
        {
            LocalSettings.Values.Remove(key);
        }

        public void SetDefaultSettings<T>(string key, T defaultValue)
        {
            Store(key, defaultValue);
        }


        public bool isKeyPresentInUserDefaults(string key)
        {
          return LocalSettings.Values.ContainsKey(key);
        }

        public void populateDefaultValues()
        {
            SetDefaultSettings<bool>("hideNumberValues", false);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged([CallerMemberName] string propName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
    }
}
