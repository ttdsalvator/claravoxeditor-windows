﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Claravox.Models
{
    public class SliderModel: BindableBase
    {
        public decimal Value { get; set; }
        public decimal Max { get; set; }
        public decimal Min { get; set; }
    }
}
