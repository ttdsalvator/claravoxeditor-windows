﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Automation.Peers;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

using Claravox.ViewModels;
using Claravox.Widgets;
using Claravox.Models;
using Claravox.MIDI;
using Windows.UI.ViewManagement;

// Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Claravox
{
    /// An empty page that can be used on its own or navigated to within a Frame.
    public sealed partial class MainPage : Page
    {
        // -------------------- variables --------------
        // handy pointer to "this"
        public static MainPage Current;
        public MidiState midiState_;
        public MidiManager midiManager_;
        public PresetManager presetManager_;
        public MidiStateViewModel midiStateViewModel_;
        public LibraryViewModel libraryViewModel_;
        public EditorSettingsAppMIDIViewModel settingsAppMIDIViewModel_;
        public EditorSettingsAppViewModel settingsAppViewModel_;
        public EditorSettingsGlobalViewModel SettingsGlobalViewModel_;
        public EditorSettingsMIDIViewModel  settingsMIDIViewModel_;
        public ViewModelLocator viewModelLocator;
        public PanelsType activePanel_;

        public enum PanelsType
        {
            panelParameter = 0,
            panelLibrary,
            panelSettings,
        };

        public enum NotifyType
        {
            StatusMessage,
            ErrorMessage
        };

        // ---------------- properties ------------------
        public List<Panel> Panels
        {
            get { return this.panels; }
        }

        // ---------------- methods ------------------
        public MainPage()
        {
            // init variables

            // This is a static public property that allows downstream pages to get a handle to the MainPage instance
            // in order to call methods that are in this class.
            Current = this;
            
            midiState_ = new MidiState();
            midiStateViewModel_ = new MidiStateViewModel(midiState_);
            midiManager_ = new MidiManager(midiState_);
            presetManager_ = new PresetManager();
            libraryViewModel_ = new LibraryViewModel();
            settingsAppMIDIViewModel_ = new EditorSettingsAppMIDIViewModel(midiManager_);
            settingsMIDIViewModel_ = new EditorSettingsMIDIViewModel (midiManager_);
            SettingsGlobalViewModel_ = new EditorSettingsGlobalViewModel(midiManager_);

            // set pointers
            midiState_.configure(midiManager_, presetManager_);
            presetManager_.configure(midiState_);

            // configure the static viewModel locator with MidiState pointer, so it's accessible throughout the modules etc...
            viewModelLocator = App.Current.Resources["viewModelLocator"] as ViewModelLocator;
            viewModelLocator.configure(midiState_);
            // assign as DataContext for future "Binding"
            this.DataContext = viewModelLocator;

            this.InitializeComponent();
            ApplicationView.PreferredLaunchViewSize = new Size(1200, 800);
            ApplicationView.PreferredLaunchWindowingMode = ApplicationViewWindowingMode.PreferredLaunchViewSize;

            showPanel(PanelsType.panelParameter);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // TODO: activePanel_ = xx;
        }
             
        /// Called whenever the user changes panel selection.  This method will navigate to the respective
        /// panel page.
        /// <param name="p"></param>
        public void showPanel(PanelsType p)
        {
            switch (p)
            {
                case PanelsType.panelParameter:
                case PanelsType.panelLibrary:
                case PanelsType.panelSettings:
                // TODO: add the other panels
                    {
                        int index = (int)p;
                        PanelFrame.Navigate(panels[index].ClassType);
                        activePanel_ = p;
                    }
                    break;
            }
        }
    
        /// Display a message to the user.
        /// This method may be called from any thread.
        /// <param name="strMessage"></param>
        /// <param name="type"></param>
        public void NotifyUser(string strMessage, NotifyType type)
        {
            // If called from the UI thread, then update immediately.
            // Otherwise, schedule a task on the UI thread to perform the update.
            if (Dispatcher.HasThreadAccess)
            {
                // UpdateStatus(strMessage, type);
            }
            else
            {
                // var task = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => UpdateStatus(strMessage, type));
            }
        }
    }
}
