﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Claravox.Models;

// Pour en savoir plus sur le modèle d'élément Contrôle utilisateur, consultez la page https://go.microsoft.com/fwlink/?LinkId=234236

namespace Claravox.Modules
{
    public sealed partial class Osc2WaveModule : UserControl
    {
        public Osc2WaveModule()
        {
            this.InitializeComponent();
        }

        public MidiState midiState
        {
            get { return (MidiState)GetValue(MidiStateProperty); }
            set { SetValue(MidiStateProperty, value); }
        }

        public static readonly DependencyProperty MidiStateProperty =
            DependencyProperty.Register("midiState", typeof(MidiState), typeof(Osc2WaveModule), new PropertyMetadata(0));
    }
}
