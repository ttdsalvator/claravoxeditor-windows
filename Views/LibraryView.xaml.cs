//*********************************************************
//
// Copyright (c) Microsoft. All rights reserved.
// This code is licensed under the MIT License (MIT).
// THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
// IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
// PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
//
//*********************************************************

using Claravox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Windows.Devices.Enumeration;
using Windows.Devices.Midi;
using Windows.Storage.Streams;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

using Claravox.ViewModels;
using Claravox.Models;
using Claravox.Data;
using Windows.Storage;
using System.Diagnostics;
using Windows.System;
using Windows.UI.Popups;

namespace Claravox
{
    public sealed partial class LibraryPanel : Page
    {
        // ---------------variables-----------------
        private MainPage rootPage;
        private PresetManager presetsManagerPtr_;

        // ---------------Methods-----------------
        /// <summary>
        /// Set the rootpage context when entering the scenario
        /// </summary>
        /// <param name="e">Event arguments</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        /// <summary>
        /// Stop the device watcher when leaving the scenario
        /// </summary>
        /// <param name="e">Event arguments</param>
        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);

            // Stop the output device watcher
            //this.midiOutDeviceWatcher.Stop();

            // Close all MidiOutPorts
            //foreach (IMidiOutPort outPort in this.midiOutPorts)
            {
                //outPort.Dispose();
            }
            // this.midiOutPorts.Clear();
        }

        /// <summary>
        /// Constructor: Start the device watcher and populate MIDI message types
        /// </summary>
        public LibraryPanel()
        {
            this.rootPage = MainPage.Current;
            presetsManagerPtr_ = rootPage.presetManager_;

            this.InitializeComponent();

            //List<FilterButtons> buttons = new List<FilterButtons>();
            //buttons.Add(new FilterButtons { ButtonContent = "b1", ButtonID = "1" });
            //buttons.Add(new FilterButtons { ButtonContent = "b2", ButtonID = "2" });
            //buttons.Add(new FilterButtons { ButtonContent = "b3", ButtonID = "3" });
            //buttons.Add(new FilterButtons { ButtonContent = "b4", ButtonID = "4" });

            //for (int i = 0; i < buttons.Count; i++)
            //{
            //    Thickness t = new Thickness(2);
            //    buttons[i].Margin = t;
            //    buttons[i].Width = 75;
            //    buttons[i].Height = 30;
            //    buttons[i].Content = i.ToString();
            //}

            // populate filters buttons
            {
                List<CheckBox> filters_buttons = new List<CheckBox>();
                List<String> filter_list = rootPage.presetManager_.AllPresetsTags;

                for (int i = 0; i < filter_list.Count; i++)
                {
                    CheckBox box = new CheckBox();
                    Thickness t = new Thickness(3);
                    box.Margin = t;
                    box.Width = 75;
                    box.Content = filter_list[i];
                    // recall checked state
                    box.IsChecked = rootPage.libraryViewModel_.ActiveFiltersList.Contains(box.Content);
                    box.Click += FilterButton_Clicked;

                    filters_buttons.Add(box);
                }

                filtersButtonsList.ItemsSource = filters_buttons;
            }

            this.DataContext = rootPage.libraryViewModel_;
        }

        /// <summary>
        /// Reset all input fields, including message type
        /// </summary>
        /// <param name="sender">Element that fired the event</param>
        /// <param name="e">Event arguments</param>
        private void resetButton_Clicked(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            presetsManagerPtr_.readPresets(false);
        }

        private void FocusedFavoriteButton_Clicked(object sender, RoutedEventArgs e)
        {
            // TODO: do in VM 
            presetsManagerPtr_.toggleFocusedPresetFavorite();
            // TODO do in VM
            rootPage.libraryViewModel_.ReloadPresets();
        }


        private void FavoriteFilterButton_Clicked(object sender, RoutedEventArgs e)
        {
            // TODO do in VM
            rootPage.libraryViewModel_.ReloadPresets();
        }

        private void FilterButton_Clicked(object sender, RoutedEventArgs e)
        {
            CheckBox box = sender as CheckBox;
            String tag = box.Content as String;

            if ((bool)box.IsChecked)
            {
                rootPage.libraryViewModel_.AddFilter(tag);
            }
            else
            {
                rootPage.libraryViewModel_.RemoveFilter(tag);
            }
        }

        async private void OpenPresetFolder_Clicked(object sender, RoutedEventArgs e)
        {
            StorageFolder storageFolder = ApplicationData.Current.LocalFolder;
            StorageFolder presetsFolder = await storageFolder.GetFolderAsync("Presets");
            await Launcher.LaunchFolderAsync(presetsFolder);
        }
        
        async private void EditTags_Clicked(object sender, RoutedEventArgs e)
        {
            StorageFolder storageFolder = ApplicationData.Current.LocalFolder;
            StorageFolder presetsFolder = await storageFolder.GetFolderAsync("Presets");
            await Launcher.LaunchFolderAsync(presetsFolder);
        }
         
        async private void Delete_Clicked(object sender, RoutedEventArgs e)
        {
            // Create the message dialog and set its content
            var messageDialog = new MessageDialog("Delete preset ?\nThis cannot be undone.");

            // Add commands and set their callbacks; both buttons use the same callback function instead of inline event handlers
            messageDialog.Commands.Add(new UICommand(
                "Cancel",
                new UICommandInvokedHandler(this.CommandInvokedHandler)));
            messageDialog.Commands.Add(new UICommand(
                "Delete",
                new UICommandInvokedHandler(this.CommandInvokedHandler)));
           
            // TODO: this doesn't seems to work (doublon text)
            //messageDialog.Title = "Delete Preset ?";

            // Set the command to be invoked when escape is pressed
            messageDialog.CancelCommandIndex = 0;
            // Set the command that will be invoked by default
            messageDialog.DefaultCommandIndex = 0;

            // Show the message dialog
            await messageDialog.ShowAsync();          
        }

        private void Delete_Confirmed()
        {
            presetsManagerPtr_.deleteFocusedPreset();
        }

        private void CommandInvokedHandler(IUICommand command)
        {
            if (command.Label == "Delete")
            {
                Delete_Confirmed();
            }
        }

        private void PresetNameBox_ContentChanged(object sender, TextChangedEventArgs e)
        {
           // no op on each keypress change (wait and process on LostFocus)
        }

        private void PresetNameBox_LostFocus(object sender, RoutedEventArgs e)
        {
            String new_name = presetTextBox.Text;
            String previous_name = presetsManagerPtr_.FocusedPresetName;
            if (new_name.Length > 0 && previous_name != new_name)
            {
                presetsManagerPtr_.FocusedPresetName = new_name;
                presetsManagerPtr_.handleRenameFavorite(previous_name, new_name);
                presetsManagerPtr_.overwritePresetFile();
            }
            else
            {
                presetTextBox.Text = previous_name;
            }
        }

        private void PresetListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //PresetViewModel preset_view_model = (PresetViewModel)presetsListView.SelectedItem;
            //{
            //    if (preset_view_model != null &&
            //        presetsManagerPtr_ != null &&
            //        preset_view_model.Name != presetsManagerPtr_.focusedPreset.Name)
            //    {
            //        Windows.Storage.StorageFile file = (Windows.Storage.StorageFile)preset_view_model.presetLite_.file_;
            //        presetsManagerPtr_.focusedPreset.Name = preset_view_model.Name; // silently update the name, to avoid async issue when recalling SelectedItem 
            //        presetsManagerPtr_.focusPreset(file.Path);
            //    }
            //}
        }
    }

    class FilterButtons : Button
    {
        public string ButtonContent { get; set; }
        public string ButtonID { get; set; }
    }
}
