//*********************************************************
//
// Copyright (c) Microsoft. All rights reserved.
// This code is licensed under the MIT License (MIT).
// THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
// IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
// PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
//
//*********************************************************

using System;
using System.Collections.Generic;
using Windows.UI.Xaml.Controls;
using Claravox;

namespace Claravox
{
    public partial class MainPage : Page
    {
        List<Panel> panels = new List<Panel>
        {
            new Panel() { Title="PARAMETERS", ClassType=typeof(ParametersView)},
            new Panel() { Title="LIBRARY", ClassType=typeof(LibraryPanel)},
            new Panel() { Title="SETTINGS", ClassType=typeof(SettingsPanel)},
        };
    }

    public class Panel
    {
        public string Title { get; set; }
        public Type ClassType { get; set; }
    }
}
