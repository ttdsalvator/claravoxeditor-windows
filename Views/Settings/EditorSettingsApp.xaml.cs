﻿using Claravox.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Claravox.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class EditorSettingsApp : Page
    {
        EditorSettingsAppViewModel mv = new EditorSettingsAppViewModel();
        public EditorSettingsApp()
        {
            this.InitializeComponent();
            this.DataContext = mv;
        }

        private void ChangeTheme(String theme)
        {
           
            if (theme == "Dark")
            {
                (Window.Current.Content as ThemeAwareFrame).AppTheme = ElementTheme.Dark;
            } else
            {
                (Window.Current.Content as ThemeAwareFrame).AppTheme = ElementTheme.Light;
            }
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            ChangeTheme((e.OriginalSource as RadioButton).Content.ToString());
        }
    }

}
