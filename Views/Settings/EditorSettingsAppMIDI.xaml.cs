﻿
using System;
using System.Collections.Generic;
using System.Text;
using Windows.Devices.Enumeration;
using Windows.Devices.Midi;
using Windows.Storage.Streams;
using Windows.UI.Core;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Controls.Primitives;

using System.Threading.Tasks;
using Windows.ApplicationModel.Core;
using System.ComponentModel;

using Claravox.ViewModels;
using Claravox.Models;
using Claravox.MIDI;
using Claravox.Utils;
using System.Collections.ObjectModel;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Claravox.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class EditorSettingsAppMIDI : Page
    {
        private MidiManager midiManagerPtr_;
        private MainPage rootPage;

        public EditorSettingsAppMIDI()
        {
            rootPage = MainPage.Current;
            midiManagerPtr_ = rootPage.midiManager_;

            this.InitializeComponent();

            this.DataContext = rootPage.settingsAppMIDIViewModel_;
        }

        /// Change the input MIDI device from which to receive messages
        /// <param name="sender">Element that fired the event</param>
        /// <param name="e">Event arguments</param>
        private async void inputDevices_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // Get the selected input MIDI device
            int selectedInputDeviceIndex = (sender as ListBox).SelectedIndex;

            // Try to create a MidiInPort
            if (selectedInputDeviceIndex < 0)
            {
                //this.rootPage.NotifyUser("Select a MIDI input device to be able to see its messages", NotifyType.StatusMessage);
                return;
            }

            string selectedDeviceName = inputDevices.SelectedItem.ToString();

            DeviceInformationCollection devInfoCollection = rootPage.midiManager_.midiDevicesWatcher.GetInputDeviceInformationCollection();
            if (devInfoCollection == null)
            {
                //this.rootPage.NotifyUser("Device not found!", NotifyType.ErrorMessage);
                return;
            }

            DeviceInformation devInfo = devInfoCollection[selectedInputDeviceIndex];
            if (devInfo == null)
            {
                //this.rootPage.NotifyUser("Device not found!", NotifyType.ErrorMessage);
                return;
            }

            var currentMidiInputDevice = await MidiInPort.FromIdAsync(devInfo.Id);
            if (currentMidiInputDevice == null)
            {
                //this.rootPage.NotifyUser("Unable to create MidiInPort from input device", NotifyType.ErrorMessage);
                return;
            }

            // We have successfully created a MidiInPort; add the device to the list of active devices, and set up message receiving
            midiManagerPtr_.setInputPort(selectedDeviceName, currentMidiInputDevice);
        }

        /// <summary>
        /// Create a new MidiOutPort for the selected device
        /// </summary>
        /// <param name="sender">Element that fired the event</param>
        /// <param name="e">Event arguments</param>
        private async void outputDevices_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // Get the selected output MIDI device
            int selectedOutputDeviceIndex = this.outputDevices.SelectedIndex;

            // Try to create a MidiOutPort
            if (selectedOutputDeviceIndex < 0)
            {
                //this.rootPage.NotifyUser("Select a MIDI output device to be able to send messages to it", NotifyType.StatusMessage);
                return;
            }

            string selectedDeviceName = outputDevices.SelectedItem.ToString();

            DeviceInformationCollection devInfoCollection = this.rootPage.midiManager_.midiDevicesWatcher.GetOutputDeviceInformationCollection();
            if (devInfoCollection == null)
            {
                //this.rootPage.NotifyUser("Device not found!", NotifyType.ErrorMessage);
                return;
            }

            DeviceInformation devInfo = devInfoCollection[selectedOutputDeviceIndex];
            if (devInfo == null)
            {
                //this.rootPage.NotifyUser("Device not found!", NotifyType.ErrorMessage);
                return;
            }

            var currentMidiOutputDevice = await MidiOutPort.FromIdAsync(devInfo.Id);
            if (currentMidiOutputDevice == null)
            {
                //this.rootPage.NotifyUser("Unable to create MidiOutPort from output device", NotifyType.ErrorMessage);
                return;
            }

            // We have successfully created a MidiOutPort; add the device to the list of active devices
            {
                midiManagerPtr_.setOutputPort(selectedDeviceName, currentMidiOutputDevice);
            }
        }
    }
}
