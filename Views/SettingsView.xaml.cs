//*********************************************************
//
// Copyright (c) Microsoft. All rights reserved.
// This code is licensed under the MIT License (MIT).
// THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
// IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
// PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
//
//*********************************************************

using Claravox;
using System;
using System.Collections.Generic;
using System.Text;
using Windows.Devices.Enumeration;
using Windows.Devices.Midi;
using Windows.Storage.Streams;
using Windows.UI.Core;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Controls.Primitives;

using System.Threading.Tasks;
using Windows.ApplicationModel.Core;
using System.ComponentModel;

using Claravox.ViewModels;
using Claravox.Models;
using Claravox.MIDI;
using Claravox.Utils;
using System.Collections.ObjectModel;
using Claravox.Views;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;
using Windows.UI;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Claravox
{
    /// An empty page that can be used on its own or navigated to within a Frame.
    public sealed partial class SettingsPanel : Page
    {
        Dictionary<string, Type> panels = new Dictionary<string, Type>();
        SolidColorBrush selectedColor = new SolidColorBrush(Colors.Yellow);
        SolidColorBrush unselectedColor = new SolidColorBrush(Colors.WhiteSmoke);

        public SettingsPanel()
        {
            this.InitializeComponent();
           
            this.setupMenuList();
            this.SettingsPanelFrame.Navigate(panels["MIDI"]);
        }

        void setupMenuList()
        {
            panels.Add("MIDI", typeof(EditorSettingsMIDI));
            panels.Add("Global", typeof(EditorSettingsGlobal));
            panels.Add("App", typeof(EditorSettingsApp));
            panels.Add("AppMIDI", typeof(EditorSettingsAppMIDI));
        }

        void gotoPage(object sender, string key)
        {
            mnApp.Foreground = unselectedColor;
            mnAppMIDI.Foreground = unselectedColor;
            mnMIDI.Foreground = unselectedColor;
            mnGlobal.Foreground = unselectedColor;
            (sender as TextBlock).Foreground = selectedColor;
            this.SettingsPanelFrame.Navigate(panels[key]);
        }


        private void mnMIDI_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {           
            gotoPage(sender,"MIDI");
        }

        private void mnGlobal_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            gotoPage(sender, "Global");
        }

        private void mnApp_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            gotoPage(sender, "App");
        }

        private void mnAppMIDI_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            gotoPage(sender, "AppMIDI");
        }
    }
}