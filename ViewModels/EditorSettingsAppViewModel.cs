﻿using Claravox.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Claravox.ViewModels
{
    public class EditorSettingsAppViewModel : BindableBase
    {
        public ObservableCollection<string> Themes { get; set; }

        public bool HideNumberValues { get; set; }

        public bool DignosticsMode { get; set; }

        // Constructor
        public EditorSettingsAppViewModel()
        {
            Themes = new ObservableCollection<string>();
            Themes.Add("Dark");
            Themes.Add("Light");
        }
    }
}
