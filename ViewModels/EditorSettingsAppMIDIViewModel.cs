﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Midi;
using System.Collections.ObjectModel;
using Claravox.MIDI;
using Claravox.Models;
namespace Claravox.ViewModels

{
   public class EditorSettingsAppMIDIViewModel : BindableBase
    {
        private MidiManager midiManagerPtr_;
        public DelegateCommand<object> ChannelClick { get; private set; }

        // TODO: currently not used because it doesn't recall list selected item properly. so ATM we still use x:Bind midiManagerPtr_
        public string SelectedInputName
        {
            get { return midiManagerPtr_.InputName; }
            set
            {
                OnPropertyChanged();
            }
        }

        // TODO: currently not used because it doesn't recall selected item properly in the ListView. 
        // So ATM we still use x:Bind midiManagerPtr_.xxx
        // Surprisingly, this binding works for TextBox, it it might probably be due to some sync issue between the update of ItemsSource and SelectedItem
        public string SelectedOutputName
        {
            get { return midiManagerPtr_.OutputName; }
            set
            {
                OnPropertyChanged();
            }
        }

        // TODO: currently not used because it doesn't recall selected item properly in the ListView. 
        // So ATM we still use x:Bind midiManagerPtr_.xxx
        // Surprisingly, this binding works for TextBox, it it might probably be due to some sync issue between the update of ItemsSource and SelectedItem
        public int SelectedChannel
        {
            get { return midiManagerPtr_.AppMidiChannel; }
            set
            {
                OnPropertyChanged();
            }
        }

        private ObservableCollection<String> inputList_ = new ObservableCollection<String>();
        public ObservableCollection<String> InputList
        {
            get { return inputList_; }
            set
            {
                inputList_ = value;
                //OnPropertyChanged();
            }
        }

        private ObservableCollection<String> outputList_ = new ObservableCollection<String>();
        public ObservableCollection<String> OutputList
        {
            get { return outputList_; }
            set
            {
                outputList_ = value;
                OnPropertyChanged();
            }
        }

        public int Channel
        {
            get
            {
                return midiManagerPtr_.AppMidiChannel;
            }

            set
            {
                if (midiManagerPtr_.AppMidiChannel != value)
                {
                    midiManagerPtr_.AppMidiChannel = value;
                    OnPropertyChanged("Channel");
                }
            }
        }

        private void executeChannelClick(object param)
        {
            int ch = Int32.Parse(param as string);
            Channel = ch;
        }

        // CONSTRUCTOR
        public EditorSettingsAppMIDIViewModel(MidiManager manager)
        {
            midiManagerPtr_ = manager;
            midiManagerPtr_.PropertyChanged += midiManager_PropertyChanged;
            ChannelClick = new DelegateCommand<object>(this.executeChannelClick);
        }

        // update from MidiManager
        private void midiManager_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "InputName")
            {
                SelectedInputName = midiManagerPtr_.InputName;
            }
            else if (e.PropertyName == "OutputName")
            {
                SelectedOutputName = midiManagerPtr_.selectedOutputName_;
            }
            else if (e.PropertyName == "InputList")
            {
                InputList.Clear();
                midiManagerPtr_.inputNamesArray.ForEach(InputList.Add);
            }
            else if (e.PropertyName == "OutputList")
            {
                OutputList.Clear();
                midiManagerPtr_.outputNamesArray.ForEach(OutputList.Add);
            }
            else if (e.PropertyName == "AppMidiChannel")
            {
                if (Channel != midiManagerPtr_.AppMidiChannel)
                {
                    Channel = midiManagerPtr_.AppMidiChannel;
                }
            }
        }
    }
}
