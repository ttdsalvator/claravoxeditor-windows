﻿using Claravox.MIDI;
using Claravox.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Claravox.ViewModels
{
    public class EditorSettingsGlobalViewModel : BindableBase
    {
        //-------- VARIABLES ------------------
        private MidiManager midiManagerPtr_;

        //-------- PROPERTIES ------------------
        public ObservableCollection<string> CVOutRange { get; set; }
        public ObservableCollection<string> CVINRange { get; set; }
        public ObservableCollection<string> PitchCVOutRange { get; set; }
        public ObservableCollection<string> PitchCVOutQuant { get; set; }
        public ObservableCollection<string> RearMute { get; set; }

        public SliderModel CVOutScale { get; set; }
        public SliderModel CVInScale { get; set; }

        public SliderModel CVInOffset { get; set; }

        public bool DignosticsMode { get; set; }

        // Constructor
        public EditorSettingsGlobalViewModel(MidiManager manager)
        {
            midiManagerPtr_ = manager;
            this.LoadInitialList();
        }

        void LoadInitialList()
        {
            CVOutRange = new ObservableCollection<string>();
            CVINRange = new ObservableCollection<string>();
            PitchCVOutRange = new ObservableCollection<string>();
            PitchCVOutQuant = new ObservableCollection<string>();
            RearMute = new ObservableCollection<string>();

            //CVOutRange
            CVOutRange.Add("+-5v");
            CVOutRange.Add("0-10v");

            //CVINRange
            CVINRange.Add("+-5v");
            CVINRange.Add("0-5v");

            //PitchCVOutRange
            PitchCVOutRange.Add("+-5v");
            PitchCVOutRange.Add("0-5v");
            PitchCVOutRange.Add("0-10v");

            //PitchCVOutQuant
            PitchCVOutQuant.Add("O");
            PitchCVOutQuant.Add("I");

            //RearMute
            RearMute.Add("Latching");
            RearMute.Add("Momentary");

            CVOutScale = new SliderModel();
            CVInScale = new SliderModel();
            CVInOffset = new SliderModel();
        }
    }
}
