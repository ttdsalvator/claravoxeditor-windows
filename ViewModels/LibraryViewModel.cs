﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Claravox.Models;
using Claravox.Data;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace Claravox.ViewModels
{
    public class LibraryViewModel : BindableBase
    {
        // --------------------- variables----------------------
        public List<PresetModel> allPresets = new List<PresetModel>();
        public ObservableCollection<PresetViewModel> filteredPresetsList_ = new ObservableCollection<PresetViewModel>();
        bool favoriteChecked_;
        bool factoryChecked_;
        bool userChecked_;
        bool focusedIsFavorite_;
        PresetViewModel selectedPreset_;
        List<String> activeFilters_ = new List<String>();

        private PresetManager presetsManagerPtr_;

        public ObservableCollection<PresetViewModel> PresetList
        {
            get { return filteredPresetsList_; }
            set { }
        }

        public PresetViewModel SelectedPreset
        {
            get { return selectedPreset_; }
            set
            {
                // On navigation to Library panel, name will be the same. Ignore those calls
                if (value != null && (selectedPreset_ == null || selectedPreset_.Name != value.Name))
                {
                    selectedPreset_ = value; OnPropertyChanged();
                    if (selectedPreset_ != null)
                    {
                        presetsManagerPtr_.focusPreset(selectedPreset_.Path);
                    }
                }
            }
        }

        // --------------------- Properties----------------------
        public List<String> ActiveFiltersList
        {
            get { return activeFilters_; }
        }

        public bool FavoriteFilterChecked
        {
            get { return favoriteChecked_; }
            set { if (favoriteChecked_ != value) { favoriteChecked_ = value; ReloadPresets(); OnPropertyChanged(); } }
        }

        public bool FactoryFilterChecked
        {
            get { return factoryChecked_; }
            set { if (factoryChecked_ != value) { factoryChecked_ = value; OnPropertyChanged(); } }
        }

        public bool UserFilterChecked
        {
            get { return userChecked_; }
            set { if (userChecked_ != value) { userChecked_ = value; OnPropertyChanged(); } }
        }

        public String FocusedPresetName
        {
            get { return presetsManagerPtr_.FocusedPresetName; }
        }

        public String FocusedPresetTags
        {
            get { return presetsManagerPtr_.focusedPreset.Tags; }
        }

        public String FocusedPresetAuthor
        {
            get { return presetsManagerPtr_.focusedPreset.Author; }
        }

        public String FocusedPresetModificationDate
        {
            get { return presetsManagerPtr_.FocusedPresetModification; }
        }

        public bool FocusedPresetFavorite
        {
            get { return presetsManagerPtr_.IsFocusedPresetFavorite(); }
            set
            {
                focusedIsFavorite_ = value;
                presetsManagerPtr_.setFocusedPresetFavorite(focusedIsFavorite_);
                OnPropertyChanged("FocusedPresetFavorite");
            }
        }

        public bool FocusedPresetIsFactory
        {
            get { return presetsManagerPtr_.focusedPreset.Factory; }
        }

        public bool FocusedPresetIsUser
        {
            get { return !FocusedPresetIsFactory; }
        }

        public bool FocusedPresetIsNotInitial
        {
            get { return !presetsManagerPtr_.focusedPreset.Name.Equals("Initial Preset"); }
        }

        //private bool FilterbuttonShouldBeChecked
        //{
        //    get { return false; }
        //}
        // ----------------- methods ----------------------
        public LibraryViewModel()
        {
            presetsManagerPtr_ = MainPage.Current.presetManager_;
            presetsManagerPtr_.PropertyChanged += presetManager_PropertyChanged;
        }

        public bool FiltersContainsTags(String[] arrayToCheck)
        {
            foreach (String tag in arrayToCheck)
            {
                if (activeFilters_.Contains(tag))
                {
                    return true;
                }
            }

            return false;
        }

        public bool FiltersContainsAuthor(String stringToCheck)
        {
            if (activeFilters_.Contains(stringToCheck))
            {
                return true;
            }

            return false;
        }

        public bool FiltersMatchFavorite(PresetLite p)
        {
            if (favoriteChecked_)
            {
                // TODO: we might remove the argument if it's always checking against it's own name
                bool isfavorite = presetsManagerPtr_.getFavorite(p.Name);
                if (isfavorite)
                {
                    return true;
                }
            }

            return false;
        }

        public void ReloadPresets()
        {
            bool is_filtered = IsFiltered();
            
            //String selected_path;
            //if (SelectedPreset != null)
            //    selected_path = SelectedPreset.Path;
            
            filteredPresetsList_.Clear();

            foreach (PresetLite p in presetsManagerPtr_.presetsLiteArray)
            {
                // add if not filtered or if tags/Author/Favourite match
                if (!is_filtered ||
                    (FiltersContainsTags(p.TagsArray) ||
                    FiltersMatchFavorite(p) ||
                    FiltersContainsAuthor(p.Author)))
                {
                    PresetViewModel preset_view_model = new PresetViewModel(p);
                    preset_view_model.Favorite = presetsManagerPtr_.getFavorite(p.Name);

                    filteredPresetsList_.Add(preset_view_model);

                    // recall selected
                    if (presetsManagerPtr_.focusedPresetFile != null &&
                        preset_view_model.Path == presetsManagerPtr_.focusedPresetFile.Path)
                    {
                        // silently  (update UI but don't load preset)
                        selectedPreset_ = preset_view_model;
                        OnPropertyChanged("SelectedPreset");
                    }
                }
            }
        }

        void updateSelectedPreset()
        {
            foreach (PresetViewModel p in filteredPresetsList_)
            {
                // recall selected
                if (presetsManagerPtr_.focusedPresetFile != null &&
                        p.Path == presetsManagerPtr_.focusedPresetFile.Path)
            {
                    selectedPreset_ = p;
                    OnPropertyChanged("SelectedPreset");
                }
            }
        }

        public void AddFilter(String key)
        {
            if (!activeFilters_.Contains(key))
            {
                activeFilters_.Add(key);
            }

            ReloadPresets();
        }

        public void RemoveFilter(String key)
        {
            if (activeFilters_.Contains(key))
            {
                activeFilters_.Remove(key);
            }

            ReloadPresets();
        }

        public bool IsFiltered()
        {
            // TODO: include favorite in activeFiltersArray ?
            return activeFilters_.Count > 0 || FavoriteFilterChecked;
        }

        private void presetManager_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "FocusedPresetName")
            {
                updateSelectedPreset();
                
                OnPropertyChanged("FocusedPresetName");
                OnPropertyChanged("FocusedPresetTags");
                OnPropertyChanged("FocusedPresetAuthor");
                OnPropertyChanged("FocusedPresetIsUser");
                OnPropertyChanged("FocusedPresetIsFactory");
                OnPropertyChanged("FocusedPresetIsNotInitial");
                OnPropertyChanged("FocusedPresetModificationDate");
            }
            else if (e.PropertyName == "FocusedPresetModification")
            {
                OnPropertyChanged("FocusedPresetFavorite");
                OnPropertyChanged("FocusedPresetTags");
                OnPropertyChanged("FocusedPresetAuthor");
                OnPropertyChanged("FocusedPresetModificationDate");
            }
            else if (e.PropertyName == "FocusedPresetFavorite")
            {
                OnPropertyChanged("FocusedPresetFavorite");
            }
            else if (e.PropertyName == "PresetListChanged")
            {
                ReloadPresets();
            }
        }
    }
}
