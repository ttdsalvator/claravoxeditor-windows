﻿using Claravox.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Claravox.Models;
using System.Runtime.CompilerServices;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace Claravox.ViewModels
{
    public class ViewModelLocator : INotifyPropertyChanged
    {
        private MidiState statePtr_;

        public ViewModelLocator()
        {
        }

        public void configure(MidiState state)
        {
            statePtr_ = state;
        }
        
        public MidiState State
        {
            get 
            {
                return statePtr_;
            }
        }

        public ObservableCollection<ParameterCC> ParamsArray
        {
            get
            {
                return statePtr_.parameterCCs;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        //private T CreateViewModel<T>() where T : new()
        //{
        //    return new T();
        //}
    }
}
