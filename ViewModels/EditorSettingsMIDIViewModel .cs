﻿using Claravox.MIDI;
using Claravox.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Claravox.ViewModels
{
    public class EditorSettingsMIDIViewModel : BindableBase
    {
        MidiManager midiManagerPtr_;
        bool showOmniIn_;
        bool showOmniOut_;
        bool is14Bits_;

        public DelegateCommand<object> ChannelInClick { get; private set; }
        public DelegateCommand<object> ChannelOutClick { get; private set; }
        public DelegateCommand<object> OutputResolutionClick { get; private set; }

        public EditorSettingsMIDIViewModel (MidiManager manager)
        {
            midiManagerPtr_ = manager;
            midiManagerPtr_.PropertyChanged += midiManager_PropertyChanged;

            ChannelInClick = new DelegateCommand<object>(this.setInputChannel);
            ChannelOutClick = new DelegateCommand<object>(this.setOutputChannel);
            OutputResolutionClick = new DelegateCommand<object>(this.setOutputResolution);
        }

        public bool IsOmniInShown
        {
            get { return showOmniIn_; }
            set { showOmniIn_ = value; OnPropertyChanged("IsOmniInShown"); }
        }

        public bool IsOmniOutShown
        {
            get { return showOmniOut_; }
            set { showOmniOut_ = value; OnPropertyChanged("IsOmniOutShown"); }
        }

        public bool Is14Bits
        {
            get
            {
                return midiManagerPtr_.Is14Bits;
            }

            set
            {
                if (midiManagerPtr_.Is14Bits != value)
                {
                    midiManagerPtr_.Is14Bits = value;
                    OnPropertyChanged("Is14Bits");
                }
            }
        }

        public int ChannelIn
        {
            get
            {
                return midiManagerPtr_.MidiInputChannel;
            }

            set
            {
                if (midiManagerPtr_.MidiInputChannel != value)
                {
                    midiManagerPtr_.MidiInputChannel = value;
                    OnPropertyChanged("ChannelIn");
                }
            }
        }

        public int ChannelOut
        {
            get
            {
                return midiManagerPtr_.MidiOutputChannel;
            }

            set
            {
                if (midiManagerPtr_.MidiOutputChannel != value)
                {
                    midiManagerPtr_.MidiOutputChannel = value;
                    OnPropertyChanged("ChannelOut");
                }
            }
        }

        private void setInputChannel(object param)
        {
            int ch = Int32.Parse(param as string);
            this.ChannelIn = ch;
        }

        private void setOutputChannel(object param)
        {
            int ch = Int32.Parse(param as string);
            this.ChannelOut = ch;
        }

        private void setOutputResolution(object param)
        {
            bool state = Boolean.Parse(param as string);
            this.Is14Bits = state; 
        }

        private void midiManager_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "MidiInputChannel")
            {
                if (ChannelIn != midiManagerPtr_.MidiInputChannel)
                {
                    ChannelIn = midiManagerPtr_.MidiInputChannel;
                }
            }
            else if (e.PropertyName == "MidiOutputChannel")
            {
                if (ChannelOut != midiManagerPtr_.MidiOutputChannel)
                {
                    ChannelOut = midiManagerPtr_.MidiOutputChannel;
                }
            }
            else if (e.PropertyName == "Is14Bits")
            {
                if (Is14Bits != midiManagerPtr_.Is14Bits)
                {
                    Is14Bits = midiManagerPtr_.Is14Bits;
                }
            }
        }
    }
}
