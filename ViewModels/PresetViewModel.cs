﻿using System;
using System.Collections.Generic;
using Claravox.Data;
using Claravox.Models;

namespace Claravox.ViewModels
{
    public class PresetViewModel : BindableBase
    {
        String name_;
        String author_;
        String tags_;
        String[] tagsArray_;
        bool isFavorite_;
        public PresetLite presetLite_;

        public PresetViewModel(PresetLite presetLite)   
        {
            presetLite_ = presetLite;
            Path = presetLite.file_.Path;
            Name = presetLite.Name;
            Author = presetLite.Author;
            Tags = presetLite.Tags;
            tagsArray_ = presetLite.TagsArray;

            Favorite = getFavorite(Name);
        }

        public string Name
        {
            get { return name_; }
            set
            {
                if (name_ != value)
                {
                    name_ = value;
                    OnPropertyChanged("Name");
                }
            }
        }

        public String[] TagsArray
        {
            get { return tagsArray_; }
            set {}
        }

        public String Path
        { get; set; }

        public string Author
        {
            get { return author_; }
            set
            {
                if (author_ != value)
                {
                    author_ = value;
                    OnPropertyChanged("Author");
                }
            }
        }

        public bool Favorite
        {
            get { return isFavorite_; }
            set
            {
                if (isFavorite_ != value)
                {
                    isFavorite_ = value;
                    //presetsManagerPtr_.toggleFavorite(Name);
                    OnPropertyChanged("IsFavorite");
                }
            }
        }

        public String Tags
        {
            get { return tags_; }
            set
            {
                tags_ = value;
                OnPropertyChanged("Tags");
            }
        }

        bool getFavorite(String name)
        {
            bool is_favorite = false;

            UserDefaults settings = UserDefaults.Current;
            if (settings != null)
                if (settings.isKeyPresentInUserDefaults("name"))
                {
                    is_favorite = settings.Read<bool>("name");
                }

            return is_favorite;
        }
    }
}
